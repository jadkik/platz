package com.platz.client;

import nu.xom.*;

public class RequestWriter{


	static String format(Element root) {
		Document doc = new Document(root);
		return doc.toXML();
	}	

	/*
	 <request action = posttopic>
	 		<title></title>
			<content></content>
			<anonymous></anonymous>
			<categories>
			<category></category>
			<category></category>
			.
			.
			.
			</categories>
	 </request>
	 */
	public static String requestPostTopic(String titleStr, long[] categoriesArray, String contentStr, boolean anonymousBool ){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","posttopic"));
		Element title = new Element("title");
		Element categories = new Element("catigories");
		Element content = new Element("content");
		Element anonymous = new Element("anonymous");

		root.appendChild(title);
		root.appendChild(content);
		root.appendChild(anonymous);
		root.appendChild(categories);

		title.appendChild(titleStr);
		content.appendChild(contentStr);
		anonymous.appendChild(Boolean.toString(anonymousBool));

		for(int i=0; i<categoriesArray.length; i++){
			Element catigory = new Element("catigory");
			catigory.appendChild(Long.toString(categoriesArray[i]));
			categories.appendChild(catigory);
		}
		return format(root);
	}

	public static String requestGetTopic(long idArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","gettopic"));
		Element id = new Element("id");

		id.appendChild(Long.valueOf(idArg).toString());
		root.appendChild(id);

		return format(root);
	}
	
	/*
	 <request action = listtopics>
	 		<filter>
				<search></search>
				<categories>
					<category></category>
					<category></category>
					.
					.
				</categories>
			</filter>
	 </request>
	 */
	public static String requestListTopicsByCategory(long[] categoryArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","listtopics"));
		Element filter = new Element("filter");
		Element search = new Element("search");
		Element categories = new Element("categories");

		root.appendChild(filter);

		filter.appendChild(search);
		filter.appendChild(categories);

		for(int i=0; i<categoryArg.length; i++){
			Element category = new Element("category");
			category.appendChild(Long.toString(categoryArg[i]));
			categories.appendChild(category);
		}
		//--
		return format(root);

	}
	public static String requestListTopicsBySearch(String searchArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","listtopics"));
		Element filter = new Element("filter");
		Element search = new Element("search");
		Element categories = new Element("categories");

		root.appendChild(filter);
		filter.appendChild(search);
		filter.appendChild(categories);
		search.appendChild(searchArg);
		//--
		return format(root);
	}
	public static String requestListTopicsByCategoryAndSearch(long[] categoryArg, String searchArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","listtopics"));
		Element filter = new Element("filter");
		Element search = new Element("search");
		Element categories = new Element("categories");

		root.appendChild(filter);
		filter.appendChild(search);
		filter.appendChild(categories);
		search.appendChild(searchArg);

		for(int i=0; i<categoryArg.length; i++){
			Element category = new Element("category");
			category.appendChild(Long.toString(categoryArg[i]));
			categories.appendChild(category);
		}
		//--
		return format(root);
	}


	public static String requestListTopics(){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","listtopics"));
		Element filter = new Element("filter");
		Element search = new Element("search");
		Element categories = new Element("categories");

		root.appendChild(filter);
		filter.appendChild(search);
		filter.appendChild(categories);
		//--
		return format(root);
	}


	/*
	 <request action = "listcategories>
	 </request>
	 */
	public static String requestListCategories(){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","listcategories"));

		return format(root);
	}




	//----

	/*
	 <request action= "loginbypassword">
			<username></username>
			<password></password>
	 </request>
	 */
	public static String requestLoginByPassword(String userNameArg, String passwordArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","loginbypassword"));

		Element userName = new Element("username");
		Element password = new Element("password");

		root.appendChild(userName);
		root.appendChild(password);

		userName.appendChild(userNameArg);
		password.appendChild(passwordArg);

		return format(root);
	}
	/*
	 <request action= "loginbycode">
			<id></id>
			<code></code>
	 </request>
	 */
	public static String requestLoginByCode(long userId, String userCode){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","loginbycode"));

		Element id = new Element("id");
		Element code = new Element("code");

		id.appendChild(Long.toString(userId));
		code.appendChild(userCode);

		root.appendChild(id);
		root.appendChild(code);

		return format(root);
	}
	
	
	/*
	 <request action= "confirmaccount">
			<username></username>
			<confirmationcode></confirmationcode>
	 </request>

	 */
	public static String requestConfirmAccount(String userNameArg, String confirmationCodeArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action", "confirmaccount"));
		
		Element userName = new Element("username");
		Element confirmaitonCode = new Element("confirmationcode");
		
		userName.appendChild(userNameArg);
		confirmaitonCode.appendChild(confirmationCodeArg);
		
		root.appendChild(userName);
		root.appendChild(confirmaitonCode);
		
		return format(root);
		
	}
	/*
	 <request action= "signup">
			<username></username>
			<password></password>
			<email></email>
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
	 </request>

	 */
	public static String requestSignUp(String userNameArg, String passwordArg,
			String emailArg, String firstNameArg, String lastNameArg, String profileTextArg){

		Element root = new Element("request");
		root.addAttribute(new Attribute("action","signup"));

		Element userName = new Element("username");
		Element password = new Element("password");
		Element email = new Element("email");
		Element firstName = new Element("firstname");
		Element lastName = new Element("lastname");
		Element profileText = new Element("profiletext");

		root.appendChild(userName);
		root.appendChild(password);
		root.appendChild(email);
		root.appendChild(firstName);
		root.appendChild(lastName);
		root.appendChild(profileText);

		userName.appendChild(userNameArg);
		password.appendChild(passwordArg);
		email.appendChild(emailArg);
		firstName.appendChild(firstNameArg);
		lastName.appendChild(lastNameArg);
		profileText.appendChild(profileTextArg);

		return format(root);
	}

	/*
	 <request action= "editprofile">
			<password></password>
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
			<college></college>
			<birthday></birhday>
			<interests></interests>
	 </request>
	 */
	public static String requestEditProfile(String passwordArg, String firstNameArg, String lastNameArg, 
			String profilTextArg, String collegeArg, String birthdayArg, String interestsArg){

		Element root = new Element("request");
		root.addAttribute(new Attribute("action","editprofile"));


		Element password = new Element("password");
		Element firstName = new Element("firstname");
		Element lastName = new Element("lastname");
		Element profileText = new Element("profiletext");
		Element college = new Element("college");
		Element birthday = new Element("birthday");
		Element interests = new Element("interests");

		root.appendChild(password);
		root.appendChild(firstName);
		root.appendChild(lastName);
		root.appendChild(profileText);
		root.appendChild(college);
		root.appendChild(birthday);
		root.appendChild(interests);

		password.appendChild(passwordArg);
		firstName.appendChild(firstNameArg);
		lastName.appendChild(lastNameArg);
		profileText.appendChild(profilTextArg);
		college.appendChild(collegeArg);
		birthday.appendChild(birthdayArg);
		interests.appendChild(interestsArg);

		return format(root);

	}


	/*
	 <request action = "userinformation">
	 	<id></id>
	 </request>
	 */
	public static String requestUserInfo(long userId){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","userinformation"));
		Element id = new Element("id");
		id.appendChild(Long.toString(userId));
		root.appendChild(id);

		return format(root);
	}

	
	/*
	 * <request action = "subscription">
	 * <topicid></topicid>
	 * <request>
	 */
	
	public static String requestSubscription(long topicId){
		
		Element root = new Element("request");
		root.addAttribute(new Attribute("action", "subscription"));
		Element id = new Element("topicid");
		id.appendChild(Long.toString(topicId));
		root.appendChild(id);
		return format(root);
	}
	
	
	/*
	 * <request action = "answer">
	 * 	<content></content>
	 * <topicid></topicid>
	 * <anonymous></anonymoud>
	 *	</request>
	 */
	
	public static String requestAnswer(String contentArg, long topicidArg, boolean anonymousArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action", "answer"));
		
		Element content = new Element("content");
		Element topicId = new Element("topicid");
		Element anonymous = new Element("anonymous");
		
		content.appendChild(contentArg);
		topicId.appendChild(Long.toString(topicidArg));
		anonymous.appendChild(Boolean.toString(anonymousArg));
		
		root.appendChild(content);
		root.appendChild(topicId);
		root.appendChild(anonymous);
		
		return format(root);
	}
	
	/*
	 * <request action = "unsubscription">
	 * <topicid></topicid>
	 * <request>
	 */
	
	public static String requestUnSubscription(long topicId){
		
		Element root = new Element("request");
		root.addAttribute(new Attribute("action", "unsubscription"));
		Element id = new Element("topicid");
		id.appendChild(Long.toString(topicId));
		root.appendChild(id);
		return format(root);
	}
	
	
	
}
