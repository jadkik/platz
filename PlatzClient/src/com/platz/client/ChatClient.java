package com.platz.client;


import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.platz.client.Client.ClientTimerTask;
import com.platz.client.exceptions.PlatzException;
import com.platz.client.exceptions.PlatzResponseException;
import com.platz.client.models.ActiveLogin;



public class ChatClient {
	
	private DatagramSocket clientSocket = null;
	public HashMap <String, String> IPtoUser = null;
	public HashMap <String, String> UsertoIP = null;
	
	public InetAddress ServerIP;
	public int ServerPort;
	private Client TcpClient;
	
	private ChatListener chatListener;
	
	ActiveLogin loggedUser;
	Timer timer;
	protected PresenceTimerTask timerTask;
	
	public static final int PRESENCE_SEND_DELAY = 10000;

	protected class PresenceTimerTask extends TimerTask {
		@Override
		public void run() {
			ActiveClient();
		}
	}
	
	public static abstract class ChatListener {
		public abstract void messageReceived(String fromUsername, String message);
	}
	
	public ChatClient(String ServerHostName, int port, Client TcpClient) throws SocketException, UnknownHostException {
		this.TcpClient = TcpClient;
		clientSocket = new DatagramSocket();
		IPtoUser = new HashMap <String, String>();
		UsertoIP = new HashMap <String, String>();
		ServerIP = InetAddress.getByName(ServerHostName);
		this.ServerPort = port;
		loggedUser = null;
	}
	
	
	public void closeConnection(){
		clientSocket.close();
	}
	

	public class ReceiveThread implements Runnable{

		@Override
		public void run() {
			while(true) {
				p2pChatReceive();
			}
		}
	}
	
	public void startSendingPresence(ActiveLogin loggedUser) {
		this.loggedUser = loggedUser;
		timer = new Timer();
		timerTask = new PresenceTimerTask();
		timer.schedule(timerTask, 0, PRESENCE_SEND_DELAY);
	}
	
	public void startReceiving(ChatListener listener) {
		this.chatListener = listener;
		new Thread(new ReceiveThread()).start();
	}
	
	//--

	public void ActiveClient() {

	
		byte[] sendData = new byte[1024];

		//-- userId + activeLogin code
		String identificationStr;

		try {
			identificationStr = "presence "+loggedUser.getUserId() + " " + loggedUser.getCode() + "\n";

			sendData = identificationStr.getBytes();

			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ServerIP, ServerPort);
			clientSocket.send(sendPacket);

		} catch (UnknownHostException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}        
	}

	public void p2pChatSend(final String username, final String message){
		
		if(!IPtoUser.containsKey(username)){
			String request = ChatController.requestIPandPortByUserName(username);
			
			TcpClient.putRequest(request, new Client.ResponseListener() {

				@Override
				public void responseReceived(String responseXml) {
					String ip;
					int port;
					
					try {
						ChatController.IPandPort a = ChatController.responseIPandPortByUserName(responseXml);
						ip = a.IP;
						port = a.port;
					} catch (PlatzResponseException ex) {
						handleError(ex);
						return;
					}
					
					String key = ip + "/" + port;
					
					IPtoUser.put(key, username);
					UsertoIP.put(username, key);
					
					p2pChatSend(ip, port, message);
				}

				@Override
				public void errorReceived(PlatzException ex) {
					System.out.println("error for ip and port");
					handleError(ex);
				}
			});
		} else {
			String[] value = IPtoUser.get(username).split("/");
			p2pChatSend(value[0], Integer.valueOf(value[1]), message);
		}
	}
	
	public void p2pChatSend(String friendIP, int friendPort, String message){
		
		byte[] sendData = message.getBytes();
		try {
			InetAddress IPAddress = InetAddress.getByName(friendIP);

			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, friendPort);
			clientSocket.send(sendPacket);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// TcpClient to request user name from server
	public void p2pChatReceive(){
		byte[]  receiveData = new byte[1024];
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		try {
			clientSocket.receive(receivePacket);
			final String receivedStr = new String(receivePacket.getData());

			String IP = receivePacket.getAddress().getHostAddress();
			String port = Integer.toString(receivePacket.getPort());
			final String key = IP + "/" + port;
			
			
			if(!IPtoUser.containsKey(key)){
				
				// call function to request user name from server
				String request = ChatController.requestUserNameByIPandPort(IP, port);
				TcpClient.putRequest(request, new Client.ResponseListener() {
					//String userName;
					@Override
					public void responseReceived(String responseXml) {
						String userName;
						try {
							userName = ChatController.responseUserNameByIpandPort(responseXml);
						} catch (PlatzResponseException ex) {
							handleError(ex);
							return;
						}
						IPtoUser.put(key, userName);
						UsertoIP.put(userName, key);
						
						chatListener.messageReceived(IPtoUser.get(key), receivedStr);
					}

					@Override
					public void errorReceived(PlatzException ex) {
						handleError(ex);
					}
				});
			} else {
				chatListener.messageReceived(IPtoUser.get(key), receivedStr);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void handleError(PlatzException ex) {
		
	}
	
}


