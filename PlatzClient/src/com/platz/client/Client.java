package com.platz.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;


import com.platz.client.exceptions.PlatzException;
import com.platz.client.models.ActiveLogin;
import com.platz.util.ErrorCode;

public class Client implements Runnable{

	static final long CONNECTION_MAX_DELAY = 5000;
	
	protected String hostname;
	protected int port;

	protected Socket socket;

	protected DataOutputStream dataout;
	protected BufferedReader datain;
	
	protected  Timer timer;
	protected SynchronousQueue<Request> queue;
	protected ErrorListener errorListener;
	protected ClientTimerTask timerTask;
	protected long lastRequest;
	
	//-- for the server to know which user is connected
	protected ActiveLogin activeLogin; 
	
	
	public ActiveLogin getActiveLogin(){
		return activeLogin;
	}
	
	public void setActiveLogin(ActiveLogin activeLogin){
		this.activeLogin = activeLogin;
	}	

	public Client(String hostname, int port, ErrorListener errorListener) {
		this.hostname = hostname;
		this.port = port;
		this.errorListener = errorListener;
		this.activeLogin = null;
	}
	
	protected void connect() throws IOException {
		try {
			socket = new Socket(hostname, port);
			
			dataout = new DataOutputStream(socket.getOutputStream());
			datain = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException ex) {
			throw ex;
		}
	}

	protected void write(String requestXml) throws IOException {
		dataout.writeBytes(requestXml.length() + "\n" + requestXml);
	}

	protected String read() throws IOException {
		String length = datain.readLine();
		if (length == null) {
			throw new IOException("Empty length");
		}
		int len = Integer.valueOf(length);

		final int AT_ONCE = 1024;

		char[] buf = new char[AT_ONCE];
		int numRead = 0;
		int numRemaining = len;
		int maxRead = Math.min(numRemaining, AT_ONCE);

		StringBuilder responseData = new StringBuilder();

		while((numRead = datain.read(buf, 0, maxRead)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			responseData.append(readData);

			buf = new char[AT_ONCE];
			numRemaining -= numRead;
			maxRead = Math.min(numRemaining, AT_ONCE);

			if (numRemaining == 0) {
				break;
			}
		}

		return responseData.toString();
	}
	
	protected void consume(Request request) throws PlatzException {
		if (!isConnected()) {
			System.out.println("Connecting...");
			try {
				this.connect();
			} catch (IOException ex) {
				throw new PlatzException(ErrorCode.CONNECTION_ERROR,
						"Could not connect to server.", ex);
			}
			
			resendLogin();
		}

		// "Reset" the timer.
		lastRequest = System.currentTimeMillis();

		// TODO debugging shit
		System.err.println("REQUEST: " + request.requestXml);
		
		String responseXml = sendAndWait(request.requestXml);
		
		// Let the listener know we got a response back
		// TODO debugging shit
		System.err.println("RESPONSE: " + responseXml);
		new Thread(new ResponseListenerThread(request.listener, responseXml)).start();
	}
	
	protected String sendAndWait(String requestXml) throws PlatzException {
		// Try to write to the server
		try {
			write(requestXml);
		} catch (IOException ex) {
			throw new PlatzException(ErrorCode.CONNECTION_ERROR,
					"Could not send request to server", ex);
		}

		// Try to read a response back
		try {
			return read();
		} catch (IOException ex) {
			throw new PlatzException(ErrorCode.CONNECTION_ERROR,
					"Could not read response from server", ex);
		}
	}
	
	protected void resendLogin() throws PlatzException {
		if (activeLogin == null) {
			System.out.println("Not sending ActiveLogin...");
			return;
		}
		System.out.println("Sending ActiveLogin " + activeLogin + " ...");
		sendAndWait(RequestWriter.requestLoginByCode(activeLogin.getUserId(), activeLogin.getCode()));
	}

	protected void close() {
		if (!isConnected()) {
			// If already closed, ignore.
			return;
		}
		
		try {
			socket.close();
		} catch (IOException ex) {
			// Ignore these errors when closing
		}
		
		socket = null;
	}

	public boolean isConnected() {
		return socket != null;
	}

	protected static class Request {
		public ResponseListener listener;
		public String requestXml;
		public boolean killThread;
		public boolean closeConnection;

		Request(String requestXml, ResponseListener listener) {
			this.listener = listener;
			this.requestXml = requestXml;
			this.killThread = false;
			this.closeConnection = false;
		}

		static Request kill() {
			Request r = new Request(null, null);
			r.killThread = true;
			return r;
		}

		static Request close() {
			Request r = new Request(null, null);
			r.closeConnection = true;
			return r;
		}
	}

	protected class ClientTimerTask extends TimerTask {
		@Override
		public void run() {
			if (scheduledExecutionTime() > lastRequest + CONNECTION_MAX_DELAY && isConnected()) {
				putClose();
			}
		}
	}

	private static class ResponseListenerThread implements Runnable {

		ResponseListener listener;
		String responseXml;
		PlatzException ex;
		
		ResponseListenerThread(ResponseListener listener, String responseXml) {
			this.listener = listener;
			this.responseXml = responseXml;
			this.ex = null;
		}
		
		ResponseListenerThread(ResponseListener listener, PlatzException ex) {
			this.listener = listener;
			this.responseXml = null;
			this.ex = ex;
		}
		
		@Override
		public void run() {
			if (ex == null) {
				this.listener.responseReceived(responseXml);
			} else {
				this.listener.errorReceived(ex);
			}
		}
		
	}
	
	public static abstract class ResponseListener {
		public abstract void responseReceived(String responseXml);
		public abstract void errorReceived(PlatzException ex);
	}
	
	public static abstract class ErrorListener {
		public abstract void handleError(PlatzException ex);
	}

	synchronized void putRequest(String requestXml, ResponseListener listener) {
		try {
			queue.put(new Request(requestXml, listener));
		} catch (InterruptedException e) {
			// Ignore this error
		}
		
		System.err.println("Elements in queue: " + queue.size());
	}

	synchronized void putKill() {
		try {
			queue.put(Request.kill());
		} catch (InterruptedException e) {
			// Ignore this error
		}
	}

	synchronized void putClose() {
		try {
			queue.put(Request.close());
		} catch (InterruptedException e) {
			// Ignore this error
		}
	}

	@Override
	public void run() {
		// Start the timer to close connection after given time
		timer = new Timer();
		timerTask = new ClientTimerTask();
		timer.schedule(timerTask, CONNECTION_MAX_DELAY, CONNECTION_MAX_DELAY);
		
		// Initialize the queue to an empty queue
		queue = new SynchronousQueue<Request>();
		
		// Keep waiting for requests from the queue
		while(true){
			Request r = null;
			try {
				System.err.println("Waiting for request...");
				r = queue.take();
				
				System.err.println("Got a request...");
				
				if (r.closeConnection) {
					System.out.println("Closing the connection...");
					this.close();
				} else if (r.killThread) {
					System.out.println("Killing the thread...");
					this.close();
					this.timer.cancel();
					return;
				} else {
					System.out.println("Consuming [" + r.requestXml.length() + "]...");
					consume(r);
				}
			} catch (PlatzException ex) {
				// If something went wrong with the connection,
				// Tell the other thread about it,
				// Close it and keep running.
				
				System.err.println("PlatZadadsasdasdasdasdksadfsjkdg");
				
				this.errorListener.handleError(ex);
				if (r != null && r.listener != null) {
					new Thread(new ResponseListenerThread(r.listener, ex)).start();
				}
				this.close();
			} catch (InterruptedException e) {
				// Ignore this error
				System.err.println("AFSJIAJSDASIDJASI");
			}
		}
	}
}
