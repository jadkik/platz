package com.platz.client;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.platz.client.exceptions.PlatzResponseException;
import com.platz.client.models.ActiveLogin;
import com.platz.client.models.Category;
import com.platz.client.models.ListTopic;
import com.platz.client.models.Topic;
import com.platz.client.models.User;
import com.platz.client.models.UserProfile;
import com.platz.util.ErrorCode;

import nu.xom.*;

public class ResponseReader {

	//-- error message format
	/*	
	<response success = "false">
		 	<error code = "123">
				"message"
			</error>
		</response>
	 */
	public static void checkError(Element root) throws PlatzResponseException{
		Attribute attr = root.getAttribute("success");
		if (attr == null) {
			throw new PlatzResponseException(ErrorCode.RESPONSE_XML_ERROR,
					"No success attribute found.");
		}
		String attrValue = attr.getValue();
		if (attrValue == null || attrValue.equals("false")){
			Element error = root.getFirstChildElement("error");
			int code;
			try {
				code = Integer.valueOf(error.getAttribute("code").getValue());
			} catch( NumberFormatException e){
				code = ErrorCode.UNKNOWN_ERROR;
			}
			String message = error.getValue();
			throw new PlatzResponseException(code, message);
		}
	}

	public static Element getRootElement(String xmlString) throws PlatzResponseException{
		Builder parser = null;
		Document doc = null;
		parser = new Builder();
		try {
			doc = parser.build(xmlString, null);
		} catch (ValidityException e) {
			throw new PlatzResponseException(ErrorCode.RESPONSE_XML_ERROR,
					"There is a validity problem with the received XML", e);
		} catch (ParsingException e) {
			throw new PlatzResponseException(ErrorCode.RESPONSE_XML_ERROR,
					"There was a problem while parsing the received XML.", e);
		} catch (IOException e) {
			throw new PlatzResponseException(ErrorCode.RESPONSE_XML_ERROR,
					"There was a problem reading the received XML.", e);
		}
		Element root = doc.getRootElement();
		return root;
	}



	/*
	 <response success = "true">
	 	<id></id>
	 </response>
	 */

	public static long responsePostTopic(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		long id = Long.valueOf(root.getChild(0).getValue());
		return id;
	}

	//-- GetTopic Response format
	/*
	 <response success = "true">
	 	<id></id>
	 	<title></title>
		<content></content>
		<anonymous></anonymous>
		<creatorid></creatorid>
		<createdOn></createdOn>
		<categories>
			<category></category>
			<category></category>
			.
			.
			.
		</categories>
	</response>
	 */
	public static Topic responseGetTopic(String xmlString) throws PlatzResponseException {
		long id;
		String title;
		String content;
		boolean anonymous;
		Date createdOn = null;
		long creatorId;
		long[] categories;
		//--
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--

		id = Long.valueOf(root.getChild(0).getValue());
		title = root.getChild(1).getValue();
		content = root.getChild(2).getValue();
		anonymous = Boolean.getBoolean(root.getChild(3).getValue());
		creatorId = Long.valueOf(root.getChild(4).getValue());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		try {
			createdOn = sdf.parse(root.getChild(5).getValue());
		} catch (ParseException e) {
			throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}
		//--

		//-- created categoryElement to get the children of categories
		Element categoriesElement = root.getChildElements().get(6);
		Elements el = categoriesElement.getChildElements("category");
		categories = new long[(int) el.size()];
		for(int i = 0; i<el.size(); i++){
			categories[i] = Long.valueOf(el.get(i).getValue());
		}

		Topic result = new Topic();
		result.setAnonymous(anonymous);
		result.setContent(content);
		result.setCreatedOn(createdOn);
		result.setCreatorId(creatorId);
		result.setId(id);
		result.setTitle(title);
		result.setCategories(categories);

		return result;
	}


	//-- getListTopics response message format
	/*
	 <response success = "true">
			<topic> 	
				<id></id>
	 			<title></title>
				<createdOn></createdOn>
			</topic>
			.
			.
			.
	</response>
	 */
	public static ListTopic[] getListTopics(String xmlString) throws PlatzResponseException{
		ListTopic[] result = null;
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		result = new ListTopic[root.getChildCount()];
		Elements Topics = root.getChildElements("topic");
		for(int i=0; i<result.length; i++){
			result[i] = new ListTopic();
			result[i].setId(Long.valueOf(Topics.get(i).getChild(0).getValue()));
			result[i].setTitle(Topics.get(i).getChild(1).getValue());

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			try {
				result[i].setCreatedOn(sdf.parse(Topics.get(i).getChild(2).getValue()));
			} catch (ParseException e) {
				throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
						"date format is wrong", e);
			}
		}
		return result;
	}

	/*
	 <response success = "true">
	 	<category>
			<name></name>
			<id></id>
		</category>
			.
			.
			.
	</response>
	 */
	public static Category[] responseListCategories(String xmlString) throws PlatzResponseException{
		Category[] result = null;
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		result = new Category[root.getChildCount()];
		Elements categories = root.getChildElements("topic");

		for(int i=0; i<result.length; i++){
			result[i] = new Category();
			result[i].setName(categories.get(i).getChild(0).getValue());
			result[i].setId(Long.valueOf(categories.get(i).getChild(1).getValue()));
		}
		return result;
	}

	/*
	<response success = "true">
			<id></id>
			<code></code>
			<createdon></createdon>
			<expireson></expiresOn>
	</response>
	 */
	public static ActiveLogin responseLogin(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		ActiveLogin result = new ActiveLogin();
		result.setUserId(Long.valueOf(root.getChild(0).getValue()));
		result.setCode(root.getChild(1).getValue());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			result.setCreatedOn(sdf.parse(root.getChild(2).getValue()));
			result.setCreatedOn(sdf.parse(root.getChild(3).getValue()));
		} catch (ParseException e) {
			throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}

		return result;
	}
	
	
	public static boolean responseSignUp(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		return true;
	}


	
	
	
	/*
	 <request success = "true">
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
			<college></college>
			<birthday></birhday>
			<interests></interests>
	 </request>
	 */
	public static User responseEditProfile(String xmlString) throws PlatzResponseException {
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		User result = new User();
		result.setFirstName(root.getChild(0).getValue());
		result.setLastName(root.getChild(1).getValue());
		result.setProfileText(root.getChild(2).getValue());
		result.setCollege(root.getChild(3).getValue());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			result.setBirthday(sdf.parse(root.getChild(4).getValue()));
		} catch (ParseException e) {
			throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}

		result.setInterests(root.getChild(5).getValue());
		return result;
	}



	/*
	 <request success = "true">
	 	<username></username>
	 	<email></email>
	 	<firstname></firstname>
	 	<lastname></lastname>
	 	<profiletext></profiletext>
	 	<birthday></birthday>
	 	<createdon></createdon>
	 	<latestanswers>
	 		<answer>
	 			<id></id>
	 			<partofcontent></date>
	 			<date></date>
	 		</answer>
	 		.
	 		.
	 		.
	 	</latestanswers>
	 	<latestquestions>
	 		<question>
	 			<id></id>
	 			<title></title>
	 			<date></date>
	 		</question>
	 		.
	 		.
	 		.
	 	</latestquestions>
	 	<latestquestions>

	 </request>
	 */
	public static UserProfile responsseUserInfo(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		UserProfile result = new UserProfile();
		result.setUsername(root.getChild(0).getValue());
		result.setEmail(root.getChild(1).getValue());
		result.setFirstName(root.getChild(2).getValue());
		result.setLastName(root.getChild(3).getValue());
		result.setProfileText(root.getChild(4).getValue());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			result.setBirthday(sdf.parse(root.getChild(5).getValue()));
		} catch (ParseException e) {
			throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		try {
			result.setCreatedOn(sdf1.parse(root.getChild(6).getValue()));
		} catch (ParseException e) {
			throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}

		Element latestAnswers = root.getChildElements().get(7);
		UserProfile.answer[] answers = new UserProfile.answer[latestAnswers.getChildCount()];
		for(int i=0; i<answers.length; i++){
			Element answer = latestAnswers.getChildElements().get(i);
			answers[i] = new UserProfile.answer();
			answers[i].setId(Long.valueOf(answer.getChild(0).getValue()));
			answers[i].setPartOfContent(answer.getChild(1).getValue());

			try {
				answers[i].setCreatedOn(sdf1.parse(answer.getChild(2).getValue()));
			} catch (ParseException e) {
				throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
						"date format is wrong", e);
			}
		}

		result.setAnswers(answers);

		Element latestQuestions = root.getChildElements().get(8);
		UserProfile.question[] questions = new UserProfile.question[latestQuestions.getChildCount()];

		for(int i=0; i < questions.length;i++){
			Element question = latestQuestions.getChildElements().get(i);
			questions[i] = new UserProfile.question();
			questions[i].setId(Long.valueOf(question.getChild(0).getValue()));
			questions[i].setTitle(question.getChild(1).getValue());
			try {
				questions[i].setCreatedOn(sdf1.parse(question.getChild(2).getValue()));
			} catch (ParseException e) {
				throw new PlatzResponseException(ErrorCode.DATE_FORMAT_ERROR,
						"date format is wrong", e);
			}
		}
		result.setQuestions(questions);
		return result;
	}
	
	/*
	 <request success = "true">
	 </request>
	 */
	
	public static boolean responseSubsicription(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		return true;
		
	}
	
	
	/*
	 <request success = "true">
	 </request>
	 */
	
	public static boolean responseAnswer(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		return true;		
	}
	
	public static boolean responseUnSubscription(String xmlString) throws PlatzResponseException{
		Element root = getRootElement(xmlString);
		//--
		checkError(root);
		//--
		return true;		
	}
		
		
}


