package com.platz.client.models;

import java.util.Date;

public class ActiveLogin {
	private long userId;
	private String code;
	private Date createdOn;
	private Date expiresOn;

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getExpiresOn() {
		return expiresOn;
	}
	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	public String toString() {
		return "{ActiveLogin userId=" + userId + "; code=" + code + "}";
	}
}
