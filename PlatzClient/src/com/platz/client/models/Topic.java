package com.platz.client.models;

import java.util.Date;


public class Topic {
	private long id;
	private String title;
	private String content;
	private Date createdOn;
	private long creatorId;
	private boolean anonymous;
	private long[] categories;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}
	public boolean isAnonymous() {
		return anonymous;
	}
	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public void setCategories(long[] categoriesArg){
		categories = new long[categoriesArg.length];
		for(int i=0; i<categoriesArg.length; i++){
			categories[i] = categoriesArg[i];
		}
	}
	public long[] getCategoires(){
		return categories;
	}

	public String toString() {
		return "Topic id=" + id +"\n"+ "title=" + title +"\n"+ "creator=" + creatorId + "\n"+ "createdOn="+ createdOn 
				+ "\n"+"anonymous="+ anonymous+ "\n" + "content=" + content + "\n" + "categories=" + categories.toString();
	}
}


