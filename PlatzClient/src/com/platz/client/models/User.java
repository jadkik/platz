package com.platz.client.models;

import java.util.Date;

public class User {
	
	private long id;
	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private String profileText;
	private Date createdOn;
	private Date lastLogin;
	private String college;
	private Date birthday;
	private String interests;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfileText() {
		return profileText;
	}
	public void setProfileText(String profileText) {
		this.profileText = profileText;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	public String getCollege(){
		return this.college;
	}
	public void setCollege(String collegeArg){
		this.college = collegeArg;
	}
	public Date getBirthday(){
		return this.birthday;
	}
	public void setBirthday(Date birthdayArg){
		this.birthday = birthdayArg;
	}
	public String getInterests(){
		return this.interests;
	}
	public void setInterests(String interestsArg){
		this.interests = interestsArg;
	}
	public String toString() {
		return "{User id=" + id + "; username=" + username + "; email=" + email + "}";
	}
}