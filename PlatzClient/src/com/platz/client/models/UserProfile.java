package com.platz.client.models;

import java.util.Date;



/*
<request success = "true">
	<username></username>
	<email></email>
	<first></firstname>
	<lastname></lastname>
	<profiletext></profiletext>
	<birthday></birthday>
	<createdon></createdon>
	<latestanswers>
		<answer>
			<id></id>
			<partofcontent></date>
			<date></date>
		</answer>
		.
		.
		.
	</latestanswers>
	<latestquestions>
		<question>
			<id></id>
			<title></title>
			<date></date>
		</question>
		.
		.
		.
	</latestquestions>
	<latestquestions>
	<lastlogin></lastlogin>

</request>
 */
public class UserProfile {
	
	 public static class answer{
		private long id;
		private String partOfContent;
		Date createdOn;

		public answer(){}
		public long getId(){
			return this.id;
		}
		public void setId(long idArg){
			this.id = idArg;
		}
		public String getPartOfContent(){
			return this.partOfContent;
		}
		public void setPartOfContent(String partOfContentArg){
			this.partOfContent = partOfContentArg;
		}
		public Date getCreatedOn(){
			return this.createdOn;
		}
		public void setCreatedOn(Date createdOnArg){
			this.createdOn = createdOnArg;
		}
		
		public String toString(){
			return ("id " + id + ", " + "partOfContent " + partOfContent + "createdOn " + createdOn);
		}
	}
	 public static class question{
		private long id;
		private String title;
		private Date createdOn;

		public question(){}

		public long getId(){
			return this.id;
		}
		public void setId(long idArg){
			this.id = idArg;
		}
		public String getTitle(){
			return this.title;
		}
		public void setTitle(String titleArg){
			this.title = titleArg;
		}
		public Date getCreatedOn(){
			return this.createdOn;
		}
		public void setCreatedOn(Date createdOnArg){
			this.createdOn = createdOnArg;
		}
		
		public String toString(){
			return ("id " + id + ", " + "title " + title + "createdOn " + createdOn);
		}
	}

	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private String profileText;
	private Date createdOn;
	private Date lastLogin;
	private String college;
	private Date birthday;
	private String interests;
	private answer[] answers;
	private question[] questions;

	public UserProfile(){}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return this.firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return this.lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfileText() {
		return this.profileText;
	}
	public void setProfileText(String profileText) {
		this.profileText = profileText;
	}
	public Date getCreatedOn() {
		return this.createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getLastLogin() {
		return this.lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getCollege(){
		return this.college;
	}
	public void setCollege(String collegeArg){
		this.college = collegeArg;
	}
	public Date getBirthday(){
		return this.birthday;
	}
	public void setBirthday(Date birthdayArg){
		this.birthday = birthdayArg;
	}
	public String getInterests(){
		return this.interests;
	}
	public void setInterests(String interestsArg){
		this.interests = interestsArg;
	}
	
	public answer[] getAnswers(){
		return this.answers;
	}
	public void setAnswers(answer[] answersArg){
		this.answers = answersArg;
	}
	
	public question[] getQuestions(){
		return this.questions;
	}
	public void setQuestions(question[] questionsArg){
		this.questions = questionsArg;
	}
	
	
	public String toString(){
		return ("username "+ this.username + "\n" + "email "+email +"\n" + 
				"firstName " + firstName+"\n" + "lastName " + lastName +"\n" + "profileText "+ profileText + "\n" 
			+ "createdOn" + createdOn + "\n" + "lastLogin " + lastLogin + "\n" + "college " + college
			+ "\n" + "birthday " + birthday + "\n" + "interest" + interests + "\n" + "answers " + answers
			+"\n" + "answers" + answers + "questions" + questions);
	} 
}


