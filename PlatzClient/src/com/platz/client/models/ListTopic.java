package com.platz.client.models;

import java.util.Date;

public class ListTopic {
	private long id;
	private String title;
	private Date createdOn;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String toString() {
		return "Topic id=" + id +"\n"+ "title=" + title +"\n"+ "createdOn="+ createdOn;
	}
}
