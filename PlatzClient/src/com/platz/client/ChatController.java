package com.platz.client;

import java.net.InetAddress;

import com.platz.client.exceptions.PlatzResponseException;

import nu.xom.Attribute;
import nu.xom.Element;

public class ChatController {
	
	/*
	 <request action= "getusernamebyipandport">
			<ip></ip>
			<port></port>
	 </request>
	 */
	
	public static String requestUserNameByIPandPort(String IPStr, String portStr){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","getusernamebyipandport"));

		Element IP = new Element("ip");
		Element port = new Element("port");

		root.appendChild(IP);
		root.appendChild(port);

		IP.appendChild(IPStr);
		port.appendChild(portStr);

		return RequestWriter.format(root);
	}
	
	/*
	 <response success = "true">
	 	<username></username>
	 </response>
	 */
	
	public static String responseUserNameByIpandPort(String xmlString) throws PlatzResponseException{
		Element root = ResponseReader.getRootElement(xmlString);
		//--
		ResponseReader.checkError(root);
		//--
		String userName = root.getChild(0).getValue();
		return userName;
	}
	
	/*
	 <request action= "getonlineusers">
	 </request>
	 */
	public static String requestListOfOnlineUsers(){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","getonlineusers"));
		
		return RequestWriter.format(root);
	}
	
	
	/*
	 <response success = "true">
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 </response>
	 */
	
	public static String[] responseListOfOnlineUsers(String xmlString) throws PlatzResponseException{
		Element root = ResponseReader.getRootElement(xmlString);
		//--
		ResponseReader.checkError(root);
		//--
		String [] userNames = new String[root.getChildCount()];
		for(int i=0; i<root.getChildCount(); i++){
			userNames[i] = root.getChild(i).getValue();
		}
		return userNames;
	}
	
	
	/*
	 <request action= "getipandportbyusername">
	 <username></username>
	 </request>
	 */
	
	
	public static String requestIPandPortByUserName(String userNameArg){
		Element root = new Element("request");
		root.addAttribute(new Attribute("action","getipandportbyusername"));

		Element userName = new Element("username");
		userName.appendChild(userNameArg);


		root.appendChild(userName);

		return RequestWriter.format(root);
	}
	
	/*
	 <response success = "true">
	 	<ip></ip>
	 	<port></port>
	 </response>
	 */

	public static IPandPort responseIPandPortByUserName(String xmlString) throws PlatzResponseException{
		Element root = ResponseReader.getRootElement(xmlString);
		//--
		ResponseReader.checkError(root);
		//--
		IPandPort id = new IPandPort();
		id.IP = root.getChild(0).getValue();
		id.port = Integer.valueOf(root.getChild(1).getValue());
		
		return id;
		
	}
	
	
	public static class IPandPort {
		public String IP;
		public int port;
	}
	
	
}
