package com.platz.client.exceptions;

import com.platz.util.ErrorCode;

/**
 * Base class for any Platz exceptions.
 * 
 * Used so that it can be catched and logged or
 * displayed to the user as necessary.
 * 
 * @author jkik
 *
 */
public class PlatzException extends Exception {

	private static final long serialVersionUID = -7231481351378714745L;

	private int code;
	protected String message;
	
	public PlatzException() {
		super();
		this.setCode(ErrorCode.UNKNOWN_ERROR);
		this.message = "";
	}
	
	public PlatzException(int code, String message) {
		super("[" + code + "] " + message);
		this.setCode(code);
		this.message = message;
	}

	public PlatzException(String message) {
		super("[" + ErrorCode.UNKNOWN_ERROR + "] " + message);
		this.setCode(ErrorCode.UNKNOWN_ERROR);
		this.message = message;
	}

	public PlatzException(int code) {
		super("[" + code + "]");
		this.setCode(code);
		this.message = "";
	}
	
	public PlatzException(Throwable cause) {
		super(cause);
		this.setCode(ErrorCode.UNKNOWN_ERROR);
		this.message = "";
	}

	public PlatzException(int code, String message, Throwable cause) {
		super("[" + code + "] " + message, cause);
		this.setCode(code);
		this.message = message;
	}
	
	public PlatzException(String message, Throwable cause) {
		super("[" + ErrorCode.UNKNOWN_ERROR + "] " + message, cause);
		this.setCode(ErrorCode.UNKNOWN_ERROR);
		this.message = message;
	}
	
	public PlatzException(int code, Throwable cause) {
		super("[" + code + "]", cause);
		this.setCode(code);
		this.message = "";
	}

	public PlatzException(int code, String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super("[" + code + "] " + message, cause,
				enableSuppression, writableStackTrace);
		this.setCode(code);
		this.message = message;
	}
	
	public PlatzException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super("[" + ErrorCode.UNKNOWN_ERROR + "] " + message, cause,
				enableSuppression, writableStackTrace);
		this.setCode(ErrorCode.UNKNOWN_ERROR);
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
