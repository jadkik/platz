package com.platz.client.exceptions;

/**
 * Platz exceptions which occur on the responses
 * received from the server.
 * 
 * Used so that it can be catched and displayed to the user.
 * 
 * @author jkik
 *
 */
public class PlatzResponseException extends PlatzException {

	private static final long serialVersionUID = 6083385491048162345L;

	public PlatzResponseException() {
		super();
	}

	public PlatzResponseException(int code, String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);
	}

	public PlatzResponseException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}

	public PlatzResponseException(int code, String message) {
		super(code, message);
	}

	public PlatzResponseException(int code, Throwable cause) {
		super(code, cause);
	}

	public PlatzResponseException(int code) {
		super(code);
	}

	public PlatzResponseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PlatzResponseException(String message, Throwable cause) {
		super(message, cause);
	}

	public PlatzResponseException(String message) {
		super(message);
	}

	public PlatzResponseException(Throwable cause) {
		super(cause);
	}
}
