package com.platz.client;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import com.platz.client.exceptions.PlatzException;
import com.platz.client.exceptions.PlatzResponseException;
import com.platz.client.models.ActiveLogin;

public class ClientMain {
	
	static ChatClient chatClient;
	static Client client;
	
	static Scanner reader;
	
	public static void main(String[] args) {
		client = new Client("localhost", 1927, new Client.ErrorListener() {
			
			@Override
			public void handleError(PlatzException ex) {
				ClientMain.handleError(ex);
			}
		});

		//--
		try {
			chatClient = new ChatClient("localhost", 1988, client);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		//--
		catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		

		
		new Thread(client).start();

		reader = new Scanner(System.in);
		
		System.out.print("Username: ");
		String username = reader.nextLine();
		System.out.print("Email: ");
		String email = reader.nextLine();
		
		String request = RequestWriter.requestSignUp(username, "secret", email, "mohamed", "maarouf", "hi there");
		client.putRequest(request, new Client.ResponseListener() {

			@Override
			public void responseReceived(String responseXml) {
				boolean success;
				try {
					success = ResponseReader.responseSignUp(responseXml);
				} catch (PlatzResponseException ex) {
					ClientMain.handleError(ex);
					return;
				}

				System.out.println("FROM SERVER (signup): " + Boolean.toString(success));
			}

			@Override
			public void errorReceived(PlatzException ex) {
				ClientMain.handleError(ex);
			}
		});

		System.out.print("Press enter when disconnected.");
		reader.nextLine();
		
		request = RequestWriter.requestLoginByPassword(username, "secret");
		
		client.putRequest(request, new Client.ResponseListener() {

			@Override
			public void responseReceived(String responseXml) {
				ActiveLogin userActiveLogin;
				try {
					userActiveLogin = ResponseReader.responseLogin(responseXml);
					client.setActiveLogin(userActiveLogin);
				} catch (PlatzResponseException ex) {
					handleError(ex);
					return;
				}
				
				System.out.println("Starting chat client for " + userActiveLogin.toString());
				
				//--
				chatClient.startSendingPresence(userActiveLogin);
				chatClient.startReceiving(new ChatClient.ChatListener() {
					
					@Override
					public void messageReceived(String fromUsername, String message) {
						continueChatting(fromUsername, message);
					}
				});
				//--
				
				System.out.println("FROM SERVER: " + Long.toString(userActiveLogin.getUserId()));
			}

			@Override
			public void errorReceived(PlatzException ex) {
				handleError(ex);
			}
		});
		
		System.out.print("Press enter when disconnected again.");
		reader.nextLine();
		
		request = ChatController.requestListOfOnlineUsers();
		
		client.putRequest(request, new Client.ResponseListener() {

			@Override
			public void responseReceived(String responseXml) {
				try {
					String[] users = ChatController.responseListOfOnlineUsers(responseXml);
					
					if (users.length > 0) {
						startChatting(users[0]);
					}
				} catch (PlatzResponseException e) {
					System.out.println("something wrong in parsing response of online users");
					e.printStackTrace();
				}
			}

			@Override
			public void errorReceived(PlatzException ex) {
				handleError(ex);
			}
		});
	}
	
	static String currentChatUser;
	
	static void startChatting(String chatUser) {
		currentChatUser = chatUser;
		System.out.println("Chatting with " + chatUser);
		while (true) {
			System.out.print("Message (to " + chatUser + "): ");
			String line = reader.nextLine();
			line = line.trim();
			
			if (line.equals("")) {
				break;
			}
			
			chatClient.p2pChatSend(chatUser, line);
		}
		
		reader.close();
		
		client.putKill();
	}

	static void continueChatting(String fromUsername, String message) {
		System.out.println(fromUsername + ":" + message);
		if (!fromUsername.equals(currentChatUser)) {
			startChatting(fromUsername);
		}
	}
	
	public static void handleError(PlatzException ex) {
		ex.printStackTrace();
		//System.err.println(ex.toString());
	}
}

