package com.platz.server.exceptions;


/**
 * Exceptions which occur on the server and need to be
 * sent back to the client to display it somehow.
 * 
 * @author jkik
 *
 */
public class PlatzClientException extends PlatzException {

	private static final long serialVersionUID = -6956067239593187049L;
	
	public PlatzClientException() {
		super();
	}
	
	public PlatzClientException(int code, String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);
	}
	
	public PlatzClientException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}
	
	public PlatzClientException(int code, String message) {
		super(code, message);
	}
	
	public PlatzClientException(int code, Throwable cause) {
		super(code, cause);
	}
	
	public PlatzClientException(int code) {
		super(code);
	}
	
	public PlatzClientException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public PlatzClientException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public PlatzClientException(String message) {
		super(message);
	}
	
	public PlatzClientException(Throwable cause) {
		super(cause);
	}
}
