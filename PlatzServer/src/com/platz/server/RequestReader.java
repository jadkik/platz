package com.platz.server;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;





import com.platz.server.exceptions.PlatzClientException;
import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.ActiveLogin;
import com.platz.server.models.Answer;
import com.platz.server.models.Topic;
import com.platz.server.models.User;
import com.platz.util.ErrorCode;

import nu.xom.*;

public class RequestReader {

	public static Element getRootElement(String xmlString)
			throws PlatzException{
		Builder parser = null;
		Document doc = null;
		parser = new Builder();
		try {
			doc = parser.build(xmlString, null);
		} catch (ValidityException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There is a validity problem with the received XML", e);
		} catch (ParsingException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There was a problem while parsing the received XML.", e);
		} catch (IOException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There was a problem reading the received XML.", e);
		}
		Element root = doc.getRootElement();
		return root;
	}

	// <request action = posttopic>
	// 		<title></title>
	//		<content></content>
	//		<anonymous></anonymous>
	//		<categories>
	//		<category></category>
	//		<category></category>
	//		.
	//		.
	//		.
	//		</categories>
	// </request>
	public static Topic readPostTopicRequest(String xmlString) throws PlatzException {
		String title;
		String content;
		boolean anonymous;
		long[] categories;
		//--
		Element root = getRootElement(xmlString);
		//--

		title = root.getChild(0).getValue();
		content = root.getChild(1).getValue();
		anonymous = Boolean.getBoolean(root.getChild(2).getValue());

		//-- created categoryElement to get the children of categories
		Element categoriesElement = root.getChildElements().get(3);
		Elements el = categoriesElement.getChildElements("category");
		categories = new long[(int) el.size()];
		for(int i = 0; i<el.size(); i++){
			categories[i] = Long.valueOf(el.get(i).getValue());
		}

		Topic result = new Topic();
		result.setAnonymous(anonymous);
		result.setContent(content);
		result.setTitle(title);
		result.setCategories(categories);

		return result;
	}


	public static long readGetTopicRequest(String xmlString) throws PlatzException{
		long id;
		//--
		Element root = getRootElement(xmlString);
		//--
		id = Long.valueOf(root.getChild(0).getValue());
		return id;
	}

	/*
	 <request action = listtopics>
	 		<filter>
				<search></search>
				<categories>
					<category></category>
					<category></category>
					.
					.
				</categories>
			</filter>
	 </request>
	 */

	public static class ListTopicsRequest {
		public String search;
		public long[] categoryIds;
	}
	
	//-- if search and categories are empty ==> list all topics
	//-- need to return searchStr
	public static ListTopicsRequest readListTopicsRequest(String xmlString) throws PlatzException{	
		long[] categoriesIds = null;
		String searchStr = null;
		//--
		Element root = getRootElement(xmlString);
		//--
		Element filter = (Element) root.getChild(0);
		Element search = (Element) filter.getChild(0);
		Element categories = (Element) filter.getChild(1);

		searchStr = search.getChild(0).getValue();
		categoriesIds = new long[categories.getChildCount()];
		for(int i=0; i<categoriesIds.length; i++){
			categoriesIds[i] = Long.valueOf(categories.getChild(i).getValue());
		}

		ListTopicsRequest request = new ListTopicsRequest();
		request.categoryIds = categoriesIds;
		request.search = searchStr;

		return request;
	}


	/*
	 <request action = "listcategories>
	 </request>
	 */

	//-- there is no data in the xml
	public static void readListCategoriesRequest(String xmlString){
		return;
	}

	/* <request action= "loginbypassword">
			<username></username>
			<password></password>
		</request>
	 */

	//-- need to return password
	

	
	public static User readLoginByPasswordRequest(String xmlString) throws PlatzException{
		User  result = null;
		//--
		Element root = getRootElement(xmlString);
		//--
		result = new User();

		result.setUsername(root.getChild(0).getValue());
		result.setPassword(root.getChild(1).getValue());
		return result;

	}


	/*
	 <request action= "loginbycode">
			<id></id>
			<code></code>
	 </request>
	 */
	public static ActiveLogin readLoginByCodeRequest(String xmlString) throws PlatzException{
		ActiveLogin result = null;
		//--
		Element root = getRootElement(xmlString);
		//--
		result = new ActiveLogin();
		result.setUserId(Long.valueOf(root.getChild(0).getValue()));
		result.setCode(root.getChild(1).getValue());

		return result;
	}

	/*
	 <request action= "confirmaccount">
			<username></username>
			<confirmationcode></confirmationcode>
	 </request>

	 */
	
	public static class ConfirmationRequest{
		public String userName;
		public String confirmationCode;
	}
		
	
	//-- need to return userName + confirmationCode
	public static ConfirmationRequest readConfirmAccountRequest(String xmlString) throws PlatzException{
		ConfirmationRequest result = null;
		
		//--
		Element root = getRootElement(xmlString);
		//--
		result = new ConfirmationRequest();
		
		result.userName = root.getChild(0).getValue();
		result.confirmationCode = root.getChild(1).getValue();
		
		return result;
	}



	/*
	 <request action= "signup">
			<username></username>
			<password></password>
			<email></email>
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
	 </request>

	 */
	//-- need to set the password
	public static User readSignUpRequest(String xmlString) throws PlatzException{
		User result = new User();
		//--
		Element root = getRootElement(xmlString);
		//--
		result.setUsername(root.getChild(0).getValue());
		result.setPassword(root.getChild(1).getValue());
		result.setEmail(root.getChild(2).getValue());
		result.setFirstName(root.getChild(3).getValue());
		result.setLastName(root.getChild(4).getValue());
		result.setProfileText(root.getChild(5).getValue());
		
		return result;
	}



	/*
	 <request action= "editprofile">
			<password></password>
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
			<college></college>
			<birthday></birhday>
			<interests></interests>
	 </request>
	 */
	//-- need to set the password
	public static User readEditProfileRequest(String xmlString) throws PlatzException{
		User result = new User();
		//--
		Element root = getRootElement(xmlString);
		//--
		result.setPassword(root.getChild(0).getValue());
		result.setFirstName(root.getChild(1).getValue());
		result.setLastName(root.getChild(2).getValue());
		result.setProfileText(root.getChild(3).getValue());
		result.setCollege(root.getChild(4).getValue());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			result.setBirthday(sdf.parse(root.getChild(5).getValue()));
		} catch (ParseException e) {
			
			throw new PlatzClientException(ErrorCode.DATE_FORMAT_ERROR,
					"date format is wrong", e);
		}
		result.setInterests(root.getChild(5).getValue());

		return result;
	}

	/*
	 <request action = "userinformation">
	 	<id></id>
	 </request>
	 */
	public static long readUserInfoRequest(String xmlString) throws PlatzException{
		long id;
		//--
		Element root = getRootElement(xmlString);
		//--
		id = Long.valueOf(root.getChild(0).getValue());
		
		return id;
	}
	
	/*
	 * <request action = "subscription">
	 * <topicid></topicid>
	 * <request>
	 */
	
	
	public static long readSubscriptionRequest(String xmlString) throws PlatzException{
		long id;
		//--
		Element root = getRootElement(xmlString);
		//--
		id = Long.valueOf(root.getChild(0).getValue());
		
		return id;
	}
	
	
	/*
	 * <request action = "answer">
	 * 	<content></content>
	 * <topicid></topicid>
	 * <anonymous></anonymoud>
	 *	</request>
	 */
	
	public static Answer readAnswerRequset(String xmlString) throws PlatzException{
		Element root = getRootElement(xmlString);
		
		Answer result = new Answer();
		//--
		result.setContent(root.getChild(0).getValue());
		result.setTopicId(Long.valueOf(root.getChild(1).getValue()));
		result.setAnonymous(Boolean.valueOf(root.getChild(2).getValue()));
		
		return result;
	}
	
	/*
	 * <request action = "unsubscription">
	 * <topicid></topicid>
	 * <request>
	 */
	
	public static long readUnSubscriptionRequest(String xmlString) throws PlatzException{
		long id;
		//--
		Element root = getRootElement(xmlString);
		//--
		id = Long.valueOf(root.getChild(0).getValue());
		
		return id;
	}
	
	
}
