package com.platz.server.models;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.platz.util.RandomCode;

public class ActiveLogin {
	private long userId;
	private String code;
	private boolean remember;
	private Date createdOn;
	private Date expiresOn;

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isRemember() {
		return remember;
	}
	public void setRemember(boolean remember) {
		this.remember = remember;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getExpiresOn() {
		return expiresOn;
	}
	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	public String toString() {
		return "{ActiveLogin userId=" + userId + "; code=" + code + "}";
	}
	
	public static String generateUniqueCode(long userId) throws NoSuchAlgorithmException {
		return RandomCode.generate(Long.toString(userId));
	}

}
