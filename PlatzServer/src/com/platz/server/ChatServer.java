package com.platz.server;

import java.io.*;
import java.net.*;
import java.util.Properties;

import com.platz.server.controllers.CoreController;
import com.platz.server.database.DatabaseConfig;
import com.platz.server.exceptions.PlatzException;

public class ChatServer {

    public static final int DEFAULT_PORT = 1988;
	
	int serverPort = DEFAULT_PORT;
	DatagramSocket serverSocket;
	
	private ServerConfig config;
    private DatabaseConfig dbconfig;
    
    CoreController ctrl;
    
    public ChatServer(ServerConfig config, DatabaseConfig dbconfig) {
    	this.config = config;
    	this.dbconfig = dbconfig;
    	
    	this.ctrl = new CoreController(this.config, this.dbconfig);
    	
    	Properties props = config.getProperties();
		
		String port = props.getProperty("chat.port");
		serverPort = (port == null? DEFAULT_PORT : Integer.valueOf(port));
    }
	
	public void connect() {
		try {
            serverSocket = new DatagramSocket(serverPort);// assign a port to the socket //
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
	}

	public void listen() {
        /* sending and receiving data is in bytes */
        byte[] receiveData = new byte[1024];
        
        while(true) {
            // receive a packet from the client, note the difference between the streams in TCP //
        	// and the packets in UDP //
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            try {
                serverSocket.receive(receivePacket);// wait for the packet to receive it //
            } catch (IOException ex) {
                ex.printStackTrace();
                continue;
            }
            
            String received = new String(receivePacket.getData());
            // get the IP address of the client //
            InetAddress ipAddress = receivePacket.getAddress();
            // get the port of the client //
            int port = receivePacket.getPort();
            
            try {
            	handleReceive(received, ipAddress, port);
            } catch (PlatzException e) {
            	// TODO what do we do with the errors in the chat server?
				e.printStackTrace();
				continue;
			} catch (IOException e) {
				// TODO what do we do with the errors in the chat server?
				e.printStackTrace();
				continue;
			}
        }
    }
	
	private void handleReceive(String received, InetAddress ipAddress, int port) 
			throws PlatzException, IOException {
		String[] parts = received.split(" ");
		
		if (parts[0].equals("presence")) {
			if (parts.length != 3) {
				return;
			}
			long userId = Long.valueOf(parts[1]);
			String activeLoginCode = parts[2];
			
			ctrl.chat.setPresence(userId, activeLoginCode, ipAddress.getHostAddress(), port);
		} else {
			// Unknown handling
			// TODO what do we do with the errors in the chat server?
			return;
		}
	}
	
	private void send(String message, InetAddress ipAddress, int port) throws IOException {
		byte[] sendData  = message.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, port);
        serverSocket.send(sendPacket);
	}
}
