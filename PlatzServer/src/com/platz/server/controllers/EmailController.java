package com.platz.server.controllers;

import javax.mail.*;
import javax.mail.internet.*;

import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.Answer;
import com.platz.server.models.Topic;
import com.platz.server.models.User;
import com.platz.util.ErrorCode;

public class EmailController extends Controller {

	public EmailController(CoreController core) {
		super(core);
	}

	/**
	 * Sends an email using the server configuration information.
	 * 
	 * @see		http://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
	 * 
	 * @param toEmails	Array of email addresses to send to.
	 * @param subject	Subject of the email message.
	 * @param body		Body of the email message.
	 * @throws PlatzException
	 */
	public void sendEmail(String[] toEmails, String subject, String body)
			throws PlatzException {
		final String username = core.config.getProperties().getProperty("mailauth.username");
		final String password = core.config.getProperties().getProperty("mailauth.password");
		
		Session session = Session.getDefaultInstance(core.config.getProperties(),
				new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		
		MimeMessage message = new MimeMessage(session);
		try {
			for (String toEmail : toEmails) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			}
			message.setSubject(subject);
			message.setText(body);
			
			Transport.send(message);
		}
		catch (MessagingException ex){
			throw new PlatzException(ErrorCode.EMAIL_ERROR,
					"Could not send email.", ex);
		}
	}

	public void sendRegistrationConfirmationCode(User user, String code) 
			throws PlatzException {
		String[] emails = {user.getEmail()};
		String body = "Dear " + user.getFirstName() + ",\n\n"
				+ "Welcome to Platz. Please verify your email to "
				+ "start using the service. Here is you confirmation "
				+ "code:\n\n"
				+ code + "\n\n"
				+ "Thank you,\n"
				+ "The Platz team.";

		sendEmail(emails, "Platz Registration", body);
	}
	
	public void sendSubscriptionEmail(User[] users, Answer answer, Topic topic, User creator) 
			throws PlatzException {
		String[] emails = new String[users.length];
		for (int i = 0; i<emails.length; i++) {
			emails[i] = users[i].getEmail();
		}
		
		String poster = (creator == null)? "(by [redacted])" : "(" + creator.getUsername() + ")";
		
		String body = "Hello!\n\n"
				+ "An answer " + poster + " has been posted in response to a topic you are subscribed to: \n"
				+ topic.getTitle() + "\n\n"
				+ "Thank you,\n"
				+ "The Platz team.";

		sendEmail(emails, "Platz Subscription", body);
	}
	
	public void sendSubscriptionEmail(User[] users, Topic topic, User creator) 
			throws PlatzException {
		String[] emails = new String[users.length];
		for (int i = 0; i<emails.length; i++) {
			emails[i] = users[i].getEmail();
		}
		
		String poster = (creator == null)? "(by [redacted])" : "(" + creator.getUsername() + ")";
		
		String body = "Hello!\n\n"
				+ "A new topic " + poster + " has been posted in a category you are subscribed to: \n"
				+ topic.getTitle() + "\n\n"
				+ "Thank you,\n"
				+ "The Platz team.";

		sendEmail(emails, "Platz Subscription", body);
	}
}
