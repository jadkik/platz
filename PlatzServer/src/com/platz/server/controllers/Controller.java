package com.platz.server.controllers;

import com.platz.server.database.Database;

public class Controller {

	protected Database db;
	protected CoreController core;
	
	public Controller(CoreController core) {
		this.core = core;
		this.db = core.db;
	}

}
