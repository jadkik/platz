package com.platz.server.controllers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

import nu.xom.*;

import com.platz.server.RequestReader;
import com.platz.server.ResponseWriter;
import com.platz.server.controllers.ChatController.Presence;
import com.platz.server.exceptions.PlatzClientException;
import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.ActiveLogin;
import com.platz.server.models.Answer;
import com.platz.server.models.Category;
import com.platz.server.models.Topic;
import com.platz.server.models.User;
import com.platz.server.models.UserProfile;
import com.platz.util.ErrorCode;

public class ServerController extends Controller {

	protected BufferedReader dataIn;
	protected DataOutputStream dataOut;

	public ServerController(CoreController core) {
		super(core);
	}

	public void setDataOut(DataOutputStream out) {
		this.dataOut = out;
	}
	
	public void setDataIn(BufferedReader in) {
		this.dataIn = in;
	}
	
	protected User requireLoggedIn() throws PlatzClientException {
		User loggedIn = core.user.getLoggedInUser();
    	if (loggedIn == null) {
    		throw new PlatzClientException(ErrorCode.USER_AUTH_ERROR,
    				"Not logged in.");
    	}
    	return loggedIn;
	}
	
	/**
	 * 
	 * @param requestXml	The request received from the client
	 * @return				false to close the connection, true otherwise
	 * @throws PlatzException
	 * @throws IOException
	 */
	public boolean handleRequest(String requestXml)
			throws PlatzException, IOException {
		Element root = getRootElement(requestXml);
        Attribute a = root.getAttribute("action");
        if (a == null) {
        	throw new PlatzClientException("No action message");
        }
        
        String action = a.getValue();
        
        // These actions don't require a logged in user
        if (handleNonLoginActions(action, requestXml)) {
        	return true;
        }
        
        User loggedIn = requireLoggedIn();
        
        if (handleLoginActions(action, requestXml, loggedIn)) {
        	return true;
        } else {
        	throw new PlatzClientException("Unknown action message");
        }
	}
	
	private boolean handleNonLoginActions(String action, String requestXml) 
			throws PlatzException, IOException {
		if (action.equals("loginbypassword")) {
        	User loginUser = RequestReader.readLoginByPasswordRequest(requestXml);
        	
        	User resultUser = new User();
        	String username = loginUser.getUsername();
        	String email = loginUser.getEmail();
        	String inputName = username; // Priority to the username
        	boolean remember = true; // TODO we should send remember in the XML request
        	
        	if (username == null && email == null) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Neither username nor email were specified.");
        	} else if (username == null) {
        		inputName = email;
        	}
        	
        	ActiveLogin activeLogin = core.user.login(resultUser, inputName, loginUser.getPassword(), remember);
        	
        	String responseXml = ResponseWriter.writeLoginResponse(activeLogin);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("loginbycode")) {
        	ActiveLogin login = RequestReader.readLoginByCodeRequest(requestXml);
        	
        	User resultUser = new User();
        	
        	ActiveLogin activeLogin = core.user.login(resultUser, login);
        	
        	String responseXml = ResponseWriter.writeLoginResponse(activeLogin);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("confirmaccount")) {
        	RequestReader.ConfirmationRequest r = RequestReader.readConfirmAccountRequest(requestXml);
        	
        	User user = new User();
        	if (!core.user.getUserByUsername(user, r.userName)) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Username not found.");
        	}
        	long userId = user.getId();
        	
        	ActiveLogin login = core.user.confirm(userId, r.confirmationCode);
        	
        	String responseXml = ResponseWriter.writeLoginResponse(login);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("signup")) {
        	User signup = RequestReader.readSignUpRequest(requestXml);
        	
        	if (!core.user.checkUsernameAvailable(signup.getUsername())) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Username already in use.");
        	}
        	if (!core.user.checkEmailAvailable(signup.getEmail())) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Email already in use.");
        	}
        	
        	if (!core.user.register(signup, signup.getPassword())) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Could not register user.");
        	}
        	
        	String responseXml = ResponseWriter.writeSignUpResponse();
        	
        	write(responseXml);
        	return true;
        }
		return false;
	}
	
	private boolean handleLoginActions(String action, String requestXml, User loggedIn) 
			throws PlatzException, IOException {
		if (action.equals("posttopic")) {
        	Topic t = RequestReader.readPostTopicRequest(requestXml);
        	
        	t.setCreatorId(loggedIn.getId());
        	
        	if (!core.topic.addTopic(t)) {
        		throw new PlatzClientException("Could not add topic.");
        	}
        	
        	String responseXml = ResponseWriter.writePostTopicResponse(t);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("gettopic")) {
        	long topicId = RequestReader.readGetTopicRequest(requestXml);
        	
        	Topic topic = new Topic();
        	if (!core.topic.getTopicById(topic, topicId)) {
        		throw new PlatzClientException("Could not get topic.");
        	}
        	
        	String responseXml = ResponseWriter.writeGetTopicResponse(topic);
        	
        	write(responseXml);
        } else if (action.equals("listtopics")) {
        	RequestReader.ListTopicsRequest r = RequestReader.readListTopicsRequest(requestXml);
        	
        	Topic[] topics = core.topic.getTopicsByCategoriesAndSearch(r.categoryIds, r.search);
        	
        	String responseXml = ResponseWriter.writeGetListTopicsResponse(topics);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("listcategories")) {
        	Category[] categories = core.topic.getCategories();
        	
        	String responseXml = ResponseWriter.writeListCategoriesResponse(categories);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("editprofile")) {
        	User edit = RequestReader.readEditProfileRequest(requestXml);
        	
        	edit.setId(loggedIn.getId());
        	
        	String password = edit.getPassword();
        	if (password != null) {
        		core.user.updateUserPassword(edit.getId(), password);
        	}
        	
        	core.user.updateUser(edit);
        	
        	User updated = new User();
        	if (!core.user.getUserById(updated, edit.getId())) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Could not get user.");
        	}
        	
        	String responseXml = ResponseWriter.writeEditProfileResponse(updated);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("userinformation")) {
        	long userId = RequestReader.readUserInfoRequest(requestXml);
        	
        	UserProfile profile = new UserProfile();
        	
        	User user = new User();
        	if (!core.user.getUserById(user, userId)) {
        		throw new PlatzClientException(ErrorCode.USER_ERROR,
        				"Could not get user.");
        	}
        	
    		profile.setUsername(user.getUsername());
    		profile.setEmail(user.getEmail());
    		profile.setFirstName(user.getFirstName());
    		profile.setLastName(user.getLastName());
    		profile.setProfileText(user.getProfileText());
    		
    		profile.setCollege(user.getCollege());
    		profile.setBirthday(user.getBirthday());
    		profile.setInterests(user.getInterests());
    		
    		profile.setCreatedOn(user.getCreatedOn());
    		profile.setLastLogin(user.getLastLogin());
    		
    		Topic[] topics = core.topic.getTopicsCreatedByUser(user);
    		UserProfile.question[] questions = new UserProfile.question[topics.length];
    		for (int i = 0; i<topics.length; i++) {
    			Topic topic = topics[i];
    			UserProfile.question q = new UserProfile.question();
    			q.setId(topic.getId());
    			q.setCreatedOn(topic.getCreatedOn());
    			q.setTitle(topic.getTitle());
    			questions[i] = q;
    		}
    		profile.setQuestions(questions);
        	
    		Answer[] answers = core.topic.getAnswersCreatedByUser(user);
    		UserProfile.answer[] qAnswers = new UserProfile.answer[answers.length];
    		for (int i = 0; i<answers.length; i++) {
    			Answer answer = answers[i];
    			UserProfile.answer ans = new UserProfile.answer();
    			ans.setId(answer.getId());
    			ans.setCreatedOn(answer.getCreatedOn());
    			String content = answer.getContent();
    			String[] lines = content.split("\n");
    			String part = "";
    			if (lines.length >= 2) {
    				part = lines[0] + "\n" + lines[1];
    			} else if (lines.length == 1) {
    				part = lines[0];
    			} else {
    				part = "[empty]";
    			}
    			ans.setPartOfContent(part);
    			qAnswers[i] = ans;
    		}
    		profile.setAnswers(qAnswers);
    		
        	String responseXml = ResponseWriter.writeUserInfoResponse(profile);
        	
        	write(responseXml);
        	return true;
        } else if (action.equals("getusernamebyipandport")) {
			Presence a = ChatController.readUserNameByIPandPortRequest(requestXml);
			
			String requestUsername = core.chat.getUsername(a.ipAddress, a.port);
			
			String responseXml = ChatController.writeUserNameByIPandPortResponse(requestUsername);
			
			write(responseXml);
			return true;
		} else if (action.equals("getipandportbyusername")) {
			String requestUsername = ChatController.readIPandPortByUserNameRequest(requestXml);
			
			Presence[] presences = core.chat.getPresence(requestUsername);
			
			if (presences.length == 0) {
				throw new PlatzClientException(ErrorCode.USER_ERROR, "User not online.");
			}
			
			String responseXml = ChatController.writeIPandPortByUserNameResponse(presences[0]);
			
			write(responseXml);
			return true;
		} else if (action.equals("getonlineusers")) {
			String[] usernames = core.chat.getOnlineUsernames(loggedIn);
			
			String responseXml = ChatController.writeListOfOnlineUsersResponse(usernames);
			
			write(responseXml);
			return true;
		}
		return false;
	}

	private Element getRootElement(String xmlString) throws PlatzException{
		Builder parser = null;
		Document doc = null;
		parser = new Builder();
		try {
			doc = parser.build(xmlString, null);
		} catch (ValidityException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There is a validity problem with the received XML", e);
		} catch (ParsingException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There was a problem while parsing the received XML.", e);
		} catch (IOException e) {
			throw new PlatzException(ErrorCode.REQUEST_XML_ERROR,
					"There was a problem reading the received XML.", e);
		}
		Element root = doc.getRootElement();
		return root;
	}
	
	void write(String responseXml) throws IOException {
		System.err.println("SEND TO CLIENT: " + responseXml);
		dataOut.writeBytes(responseXml.length() + "\n" + responseXml);
	}

	public void sendErrorResponse(PlatzClientException ex) 
			throws PlatzException, IOException {
		write(ResponseWriter.writeErrorResponse(ex));
	}

	public void sendServerErrorResponse() 
			throws PlatzException, IOException {
		write(ResponseWriter.writeErrorResponse(new PlatzClientException()));
	}
}
