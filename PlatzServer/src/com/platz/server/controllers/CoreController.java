package com.platz.server.controllers;

import com.platz.server.ServerConfig;
import com.platz.server.database.Database;
import com.platz.server.database.DatabaseConfig;

public class CoreController {

	protected DatabaseConfig dbconfig;
	protected Database db;
	
	public ServerConfig config;
	
	public ServerController server;
	
	public ChatController chat;
	
	public UserController user;
	public EmailController email;
	public TopicController topic;
	
	public CoreController(ServerConfig config, DatabaseConfig dbconfig) {
		this.config = config;
		this.dbconfig = dbconfig;
		this.db = new Database(dbconfig);
		
		server = new ServerController(this);
		
		chat = new ChatController(this);
		
		user = new UserController(this);
		email = new EmailController(this);
		topic = new TopicController(this);
	}
}
