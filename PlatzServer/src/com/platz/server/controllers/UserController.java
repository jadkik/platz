package com.platz.server.controllers;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Properties;

import com.platz.server.exceptions.PlatzException;
import com.platz.server.exceptions.PlatzClientException;
import com.platz.server.models.ActiveLogin;
import com.platz.server.models.User;
import com.platz.util.BCrypt;
import com.platz.util.ErrorCode;
import com.platz.util.RandomCode;

public class UserController extends Controller {
	private User loggedInUser;
	
	public UserController(CoreController core) {
		super(core);
	}
	
	/**
	 * Logs in a user if the username/email and passwords match.
	 * The logged in user is saved in the controller.
	 * 
	 * If a user is already logged in, it will be logged out,
	 * regardless of whether the login operation is successful or not.
	 * 
	 * Returns null if the username/password do not match.
	 * 
	 * @param user		The user object.
	 * @param inputName	The username or email of the user.
	 * @param password	The candidate unencrypted password of the user.
	 * @param remember	Whether the active login code expires in a long time or not.
	 * @return			The ActiveLogin object or null if invlaid login details
	 * @throws PlatzException 
	 */
	public ActiveLogin login(User user, String inputName, String password, boolean remember) 
			throws PlatzException {
		this.loggedInUser = null;
		
		String query = "SELECT id,username,email,password "
				+ "FROM users WHERE "
				+ "username = ? OR email = ?";
		
		long userId = -1;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, inputName);
			ps.setString(2, inputName);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				long id = rs.getLong(1);
				String dbPassword = rs.getString(4);
				if (checkPassword(password, dbPassword)) {
					userId = id;
					break;
				}
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
		
		if (userId == -1) {
			return null;
		}
		
		if (!getUserById(user, userId)) {
			return null;
		}
		
		updateLastLoginDate(user);
		
		if (!user.isActive()) {
			throw new PlatzClientException(
					ErrorCode.USER_CONFIRM_ERROR,
					"You need to confirm your email to login.");
		}
		
		ActiveLogin login = createActiveLogin(user, remember);
		
		this.loggedInUser = user;
		
		return login;
	}
	
	/**
	 * Logs in a user if the ActiveLogin code is assigned to
	 * the given user and it did not expire.
	 * 
	 * @param user		The User object.
	 * @param login		The ActiveLogin object.
	 * @return			The same ActiveLogin or null if not found.
	 * @throws PlatzException
	 */
	public ActiveLogin login(User user, ActiveLogin login) 
			throws PlatzException {
		this.loggedInUser = null;
		
		String query = "SELECT al.user_id,al.code,al.created_on,al.expires_on "
				+ "FROM active_logins al "
				+ "LEFT JOIN users u ON u.id = al.user_id "
				+ "WHERE "
				+ "u.active AND " // Only active users can login this way
				+ "al.user_id = ? AND al.code = ? AND al.expires_on > NOW() "
				+ "LIMIT 1";
		
		long userId = -1;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, login.getUserId());
			ps.setString(2, login.getCode());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				userId = rs.getLong(1);
				
				login.setUserId(userId);
				login.setCode(rs.getString(2));
				login.setCreatedOn(rs.getTime(3));
				login.setExpiresOn(rs.getTimestamp(4));
			} else {
				return null;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
		
		if (userId == -1) {
			return null;
		}
		
		if (!getUserById(user, userId)) {
			return null;
		}
		
		// No need to create a new ActiveLogin
		getActiveLogin(login, userId, login.getCode());
		
		if (!user.isActive()) {
			throw new PlatzClientException(
					ErrorCode.USER_CONFIRM_ERROR,
					"You need to confirm your email to login.");
		}
		
		updateLastLoginDate(user, login);
		
		this.loggedInUser = user;
		
		return login;
	}
	
	/**
	 * Add a user to the database with the given information.
	 * 
	 * Returns false if the user is not found after inserting it. 
	 * 
	 * @param user		The User object.
	 * @param password	The password of the user.
	 * @return			true on success, false on failure
	 * @throws PlatzException 
	 */
	public boolean register(User user, String password) 
			throws PlatzException {
		this.loggedInUser = null;
		
		String query = "INSERT INTO users "
				+ "(username, email, password, first_name, last_name, profile_text) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		
		String hashedPassword = hashPassword(password);
		long userId = -1;
		
		ResultSet generatedKeys = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getEmail());
			ps.setString(3, hashedPassword);
			ps.setString(4, user.getFirstName());
			ps.setString(5, user.getLastName());
			ps.setString(6, user.getProfileText());
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
			
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				userId = generatedKeys.getLong(1);
	        } else {
	        	throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, generatedKeys);
		}
		
		if (!getUserById(user, userId)) {
			return false;
		}
		
		handleConfirmation(user);
		
		return true;
	}
	
	/**
	 * Update the user's password.
	 * 
	 * @param userId	The user's id.
	 * @param password	The new password.
	 * @throws PlatzException
	 */
	public void updateUserPassword(long userId, String password) 
			throws PlatzException {
		String query = "UPDATE users SET "
				+ "password = ? "
				+ "WHERE id = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, password);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Update the user's information.
	 * 
	 * @param user		The User object
	 * @throws PlatzException
	 */
	public void updateUser(User user) 
			throws PlatzException {
		String query = "UPDATE users SET "
				+ "first_name = ?, "
				+ "last_name = ?, "
				+ "profile_text = ?, "
				+ "college = ?, "
				+ "birthday = ?, "
				+ "interests = ? "
				+ "WHERE id = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getProfileText());
			ps.setString(4, user.getCollege());
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date birthday = user.getBirthday();
			if (birthday == null) {
				ps.setNull(5, java.sql.Types.DATE);
			} else {
				ps.setString(5, sdf.format(birthday));
			}
			ps.setString(6, user.getInterests());
			ps.setLong(7, user.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Generate and add a new confirmation code for the given user.
	 * 
	 * @param user	The User object.
	 * @return		The confirmation code.
	 * @throws PlatzException 
	 */
	public String generateConfirmationCode(User user) 
			throws PlatzException {
		String code;
		try {
			String fullCode = RandomCode.generate(Long.toString(user.getId()));
			code = fullCode.substring(0, 8);
		} catch (NoSuchAlgorithmException ex) {
			// Should never happen
			throw new PlatzException(ex);
		}
		
		String interval = "INTERVAL 7 DAY";
		
		String query = "INSERT INTO user_confirmations "
				+ "(code, user_id, requested_on, expires_on) "
				+ "VALUES (?, ?, NOW(), DATE_ADD(NOW(), " + interval + "))";
		
		long userId = user.getId();
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, code);
			ps.setLong(2, userId);
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
		
		return code;
	}
	
	/**
	 * Checks if the confirmation code is associated to the user.
	 * 
	 * @param userId	The user's id.
	 * @param code		The confirmation code.
	 * @return			new ActiveLogin on match, null otherwise
	 * @throws PlatzException
	 */
	public ActiveLogin confirm(long userId, String code) 
			throws PlatzException {
		this.loggedInUser = null;
		
		String query = "SELECT uc.user_id,uc.code,uc.requested_on,uc.expires_on "
				+ "FROM user_confirmations uc "
				+ "LEFT JOIN users u ON u.id = uc.user_id "
				+ "WHERE "
				+ "NOT u.active AND " // Only inactive users can confirm emails
				+ "uc.user_id = ? AND uc.code = ? AND uc.expires_on > NOW() "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		boolean match = false;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, userId);
			ps.setString(2, code);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				// There was a match
				match = true;
			} else {
				// There was no match
				match = false;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
		
		if (!match) {
			return null;
		}
		
		User user = new User();
		if (!getUserById(user, userId)) {
			return null;
		}
		
		updateLastLoginDate(user);
		setUserActive(user, true);
		
		ActiveLogin login = createActiveLogin(user, false);
		
		this.loggedInUser = user;
		
		return login;
	}
	
	public void setUserActive(User user, boolean isActive) 
			throws PlatzException {
		String query = "UPDATE users SET active = ? "
				+ "WHERE id = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setBoolean(1, isActive);
			ps.setLong(2, user.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Checks if a username is already used in the database.
	 * 
	 * @param username	The username to check for.
	 * @return			true if it is not already used, false otherwise
	 * @throws PlatzException 
	 */
	public boolean checkUsernameAvailable(String username) 
			throws PlatzException {
		return checkFieldAvailable("username", username);
	}
	
	/**
	 * Checks if an email is already used in the database.
	 * 
	 * @param email		The email to check for.
	 * @return			true if it is not already used, false otherwise
	 * @throws PlatzException 
	 */
	public boolean checkEmailAvailable(String email) 
			throws PlatzException {
		return checkFieldAvailable("email", email);
	}
	
	/**
	 * Checks if a field (email,username) is already used in
	 * the database.
	 * 
	 * @param field		The field to check.
	 * @param value		The value of the field to check for.
	 * @return			true if it is not already used, false otherwise
	 * @throws PlatzException 
	 */
	protected boolean checkFieldAvailable(String field, String value) 
			throws PlatzException {
		String query = "SELECT id,username,email,created_on,last_login "
				+ "FROM users WHERE "
				+ field + " = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, value);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				// A result was found
				return false;
			} else {
				// No result was found
				return true;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Generates and sends a confirmation email if enabled or sets the user
	 * as active.
	 * 
	 * @param user	The User object.
	 * @throws PlatzException
	 */
	protected void handleConfirmation(User user) throws PlatzException {
		// Check if confirmation is enabled in config
		// It is false by default
		Properties props = core.config.getProperties();
		String confirmation = props.getProperty("user.confirmation", "false");
		
		boolean enabled = confirmation.equals("true");
		
		if (enabled) {
			// Generate and save code, then send email
			String code = generateConfirmationCode(user);
			core.email.sendRegistrationConfirmationCode(user, code);
		} else {
			// Skip if not enabled explicitly
			// And set user as active
			setUserActive(user, true);
		}
	}
	
	/**
	 * Get the user identified by his id.
	 * 
	 * @param user	The User object.
	 * @param id	The user id.
	 * @return		true on success, false if not found
	 * @throws PlatzException 
	 */
	public boolean getUserById(User user, long id) 
			throws PlatzException {
		String query = "SELECT id,username,email,first_name,last_name,profile_text,college,birthday,interests,active,created_on,last_login "
				+ "FROM users WHERE "
				+ "id = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				user.setId(id);
				user.setUsername(rs.getString(2));
				user.setEmail(rs.getString(3));
				user.setFirstName(rs.getString(4));
				user.setLastName(rs.getString(5));
				user.setProfileText(rs.getString(6));
				
				user.setCollege(rs.getString(7));
				user.setBirthday(rs.getTimestamp(8));
				user.setInterests(rs.getString(9));
				
				user.setActive(rs.getBoolean(10));
				user.setCreatedOn(rs.getTimestamp(11));
				user.setLastLogin(rs.getTimestamp(12));
				
				return true;
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get the user identified by his username.
	 * 
	 * @param user		The User object.
	 * @param username	The username.
	 * @return			true on success, false if not found
	 * @throws PlatzException 
	 */
	public boolean getUserByUsername(User user, String username) 
			throws PlatzException {
		String query = "SELECT id,username,email,first_name,last_name,profile_text,active,created_on,last_login "
				+ "FROM users WHERE "
				+ "username = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, username);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				user.setId(rs.getLong(1));
				user.setUsername(rs.getString(2));
				user.setEmail(rs.getString(3));
				user.setFirstName(rs.getString(4));
				user.setLastName(rs.getString(5));
				user.setProfileText(rs.getString(6));
				
				user.setActive(rs.getBoolean(7));
				user.setCreatedOn(rs.getTimestamp(8));
				user.setLastLogin(rs.getTimestamp(9));
				
				return true;
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Returns the logged-in User object
	 * or null if no user is logged in.
	 * 
	 * @return	The User object or null.
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}
	
	/**
	 * Returns true if the user is logged in.
	 * 
	 * @return	true if logged in, false otherwise
	 */
	public boolean isLoggedIn() {
		return loggedInUser != null;
	}
	
	/**
	 * Create a new ActiveLogin with a random code
	 * that expires in 1 month.
	 * 
	 * @param user		The logged in User object.
	 * @param remember	Whether the active login code expires in a long time or not.
	 * @return			The new ActiveLogin object.
	 * @throws PlatzException
	 */
	protected ActiveLogin createActiveLogin(User user, boolean remember) 
			throws PlatzException {
		String interval = remember? "INTERVAL 31 DAY" : "INTERVAL 1 HOUR";
		
		String query = "INSERT INTO active_logins "
				+ "(code, user_id, remember, created_on, expires_on) "
				+ "VALUES (?, ?, ?, NOW(), DATE_ADD(NOW(), " + interval + "))";
		
		long userId = user.getId();
		
		String code;
		try {
			code = ActiveLogin.generateUniqueCode(userId);
		} catch (NoSuchAlgorithmException ex) {
			// Should never happen
			throw new PlatzException(ex);
		}
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, code);
			ps.setLong(2, userId);
			ps.setBoolean(3, remember);
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
		
		ActiveLogin login = new ActiveLogin();
		getActiveLogin(login, userId, code);
		
		return login;
	}
	
	/**
	 * Update the last login date in the database for a user.
	 * 
	 * @param User	The User object.
	 * @throws PlatzException 
	 */
	protected void updateLastLoginDate(User user) 
			throws PlatzException {
		String query = "UPDATE users SET last_login = NOW() "
				+ "WHERE id = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, user.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Update the last login date in the database for a user.
	 * 
	 * @param User	The User object.
	 * @throws PlatzException 
	 */
	protected void updateLastLoginDate(User user, ActiveLogin login) 
			throws PlatzException {
		updateLastLoginDate(user);
		
		String interval = login.isRemember()? "INTERVAL 31 DAY" : "INTERVAL 1 HOUR";
		
		String query = "UPDATE active_logins SET expires_on = DATE_ADD(NOW(), " + interval + ") "
				+ "WHERE user_id = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, user.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	protected void getActiveLogin(ActiveLogin login, long userId, String code) throws PlatzException {
		String select_query = "SELECT code,user_id,remember,created_on,expires_on "
				+ "FROM active_logins "
				+ "WHERE code = ? AND user_id = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(select_query);
			ps.setString(1, code);
			ps.setLong(2, userId);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				login.setCode(rs.getString(1));
				login.setUserId(rs.getLong(2));
				login.setRemember(rs.getBoolean(3));
				login.setCreatedOn(rs.getTimestamp(4));
				login.setExpiresOn(rs.getTimestamp(5));
			} else {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"Did not find the added active_login row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Hash a password for the first time, before adding to the database.
	 * 
	 * @param password	The unencrypted password.
	 * @return			The hashed password
	 */
	protected String hashPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
	
	/**
	 * Check that an unencrypted password (supplied by user)
	 * matches one that has previously been hashed
	 * (saved in the database).
	 * 
	 * @param candidate		The supplied candidate password.
	 * @param hashed		The hashed correct password.
	 * @return				true if it matches, false otherwise
	 */
	protected boolean checkPassword(String candidate, String hashed) {
		return BCrypt.checkpw(candidate, hashed);
	}
}
