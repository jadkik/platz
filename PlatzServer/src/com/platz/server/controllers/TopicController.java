package com.platz.server.controllers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.Answer;
import com.platz.server.models.Category;
import com.platz.server.models.Topic;
import com.platz.server.models.User;
import com.platz.util.ErrorCode;

public class TopicController extends Controller {
	public TopicController(CoreController core) {
		super(core);
	}
	
	/**
	 * Adds a topic to the database.
	 * 
	 * Returns false if the topic is not found after inserting it.
	 * 
	 * @param topic		The topic (question).
	 * @return 			true on success, false otherwise 
	 * @throws PlatzException 
	 */
	public boolean addTopic(Topic topic) throws PlatzException {
		String query = "INSERT INTO topics "
				+ "(title, content, creator_id, anonymous) "
				+ "VALUES (?, ?, ?, ?)";
		
		long topicId = -1;
		
		ResultSet generatedKeys = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, topic.getTitle());
			ps.setString(2, topic.getContent());
			ps.setLong(3, topic.getCreatorId());
			ps.setBoolean(4, topic.isAnonymous());
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
			
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				topicId = generatedKeys.getLong(1);
	        } else {
	        	throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, generatedKeys);
		}
		
		return getTopicById(topic, topicId);
	}
	
	/**
	 * Sets the categories to which a topic belongs.
	 * This method will clear old categories to which
	 * the topic belongs and add the new ones.
	 * 
	 * @param topic			The Topic object.
	 * @param categoryIds	An array of new category ids 
	 * @throws PlatzException 
	 * @throws SQLException 
	 */
	public void setTopicCategories(Topic topic, long[] categoryIds) 
			throws PlatzException {
		String delete_query = "DELETE FROM topic_categories "
				+ "WHERE topic_id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(delete_query);
			ps.setLong(1, topic.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
		
		String insert_query = "INSERT INTO topic_categories "
				+ "(topic_id, category_id) "
				+ "VALUES (?, ?)";
		
		ps = null;
		try {
			ps = db.prepareStatement(insert_query);
			for (long categoryId : categoryIds) {
				ps.setLong(1, topic.getId());
				ps.setLong(2, categoryId);
				
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Adds an answer to a topic to the database.
	 * 
	 * Returns false if the answer is not found after inserting it.
	 * 
	 * @param answer	The Answer object.
	 * @return			true on success, false otherwise
	 * @throws PlatzException 
	 */
	public boolean addAnswer(Answer answer) throws PlatzException {
		String query = "INSERT INTO answers "
				+ "(content, topic_id, creator_id, anonymous) "
				+ "VALUES (?, ?, ?, ?)";
		
		long answerId = -1;
		
		ResultSet generatedKeys = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, answer.getContent());
			ps.setLong(2, answer.getTopicId());
			ps.setLong(3, answer.getCreatorId());
			ps.setBoolean(4, answer.isAnonymous());
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
			
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				answerId = generatedKeys.getLong(1);
	        } else {
	        	throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, generatedKeys);
		}
		
		return getAnswerById(answer, answerId);
	}
	
	/**
	 * Subscribe the user to a topic.
	 * 
	 * @param userId	The user id.
	 * @param topicId	The topic id.
	 * @throws PlatzException 
	 */
	public void subscribeUserToTopic(long userId, long topicId) 
			throws PlatzException {
		// INSERT IGNORE because if (topic, user) already exists, no problem
		String query = "INSERT IGNORE INTO topic_subscriptions "
				+ "(topic_id, user_id) "
				+ "VALUES (?, ?)";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, topicId);
			ps.setLong(2, userId);
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Subscribe the user to a category.
	 * 
	 * @param userId		The user id.
	 * @param categoryId	The category id.
	 * @throws PlatzException 
	 */
	public void subscribeUserToCategory(long userId, long categoryId) 
			throws PlatzException {
		// INSERT IGNORE because if (cat, user) already exists, no problem
		String query = "INSERT IGNORE INTO category_subscriptions "
				+ "(category_id, user_id) "
				+ "VALUES (?, ?)";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, categoryId);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Unsubscribe the user from a topic.
	 * 
	 * @param userId	The user id.
	 * @param topicId	The topic id.
	 * @throws PlatzException 
	 */
	public void unsubscribeUserFromTopic(long userId, long topicId)
			throws PlatzException {
		String query = "DELETE FROM topic_subscriptions "
				+ "WHERE topic_id = ? AND user_id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, topicId);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Unsubscribe a user from a category.
	 * 
	 * @param userId		The user id.
	 * @param categoryId	The category id.
	 * @throws PlatzException 
	 */
	public void unsubscribeUserFromCategory(long userId, long categoryId) 
			throws PlatzException {
		String query = "DELETE FROM category_subscriptions "
				+ "WHERE category_id = ? AND user_id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, categoryId);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Cast a positive vote for a topic.
	 * A vote can only be cast once (either positive or negative).
	 * 
	 * @param userId	The voter user's id.
	 * @param topicId	The topic to vote positively on.
	 * @throws PlatzException
	 */
	public void votePositiveForTopic(long userId, long topicId) 
			throws PlatzException {
		vote("topic", true, userId, topicId);
	}
	
	/**
	 * Cast a negative vote for a topic.
	 * A vote can only be cast once (either positive or negative).
	 * 
	 * @param userId	The voter user's id.
	 * @param topicId	The topic to vote negatively on.
	 * @throws PlatzException
	 */
	public void voteNegativeForTopic(long userId, long topicId) 
			throws PlatzException {
		vote("topic", false, userId, topicId);
	}
	
	/**
	 * Cast a positive vote for an answer.
	 * A vote can only be cast once (either positive or negative).
	 * 
	 * @param userId	The voter user's id.
	 * @param answerId	The answer to vote positively on.
	 * @throws PlatzException
	 */
	public void votePositiveForAnswer(long userId, long answerId) 
			throws PlatzException {
		vote("answer", true, userId, answerId);
	}
	
	/**
	 * Cast a negative vote for an answer.
	 * A vote can only be cast once (either positive or negative).
	 * 
	 * @param userId	The voter user's id.
	 * @param answerId	The answer to vote negatively on.
	 * @throws PlatzException
	 */
	public void voteNegativeForAnswer(long userId, long answerId) 
			throws PlatzException {
		vote("answer", false, userId, answerId);
	}
	
	/**
	 * Cast a (positive or negative) vote on an answer or topic.
	 * Only used internally by other methods.
	 * 
	 * @param field		One of "answer" or "topic".
	 * @param positive	true for a positive vote, false for a negative.
	 * @param userId	The voter user's id.
	 * @param fieldId	The id of the thing to vote on.
	 * @throws PlatzException
	 */
	protected void vote(String field, boolean positive,
			long userId, long fieldId) throws PlatzException {
		String query = "INSERT IGNORE INTO " + field + "_votes "
				+ "(" + field + "_id, user_id, positive) "
				+ "VALUES (?, ?, ?)";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, fieldId);
			ps.setLong(2, userId);
			ps.setBoolean(3, positive);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Cancels the positive and negative vote for a topic.
	 * 
	 * @param userId	The voter user's id.
	 * @param topicId	The topic id to clear votes on.
	 * @throws PlatzException
	 */
	public void clearVoteForTopic(long userId, long topicId) 
			throws PlatzException {
		clearVote("topic", userId, topicId);
	}
	
	/**
	 * Cancels the positive and negative vote for an answer.
	 * 
	 * @param userId	The voter user's id.
	 * @param answerId	The answer id to clear votes on.
	 * @throws PlatzException
	 */
	public void clearVoteForAnswer(long userId, long answerId) 
			throws PlatzException {
		clearVote("answer", userId, answerId);
	}
	
	/**
	 * Clear a vote on an answer or topic.
	 * Only used internally by other methods.
	 * 
	 * @param field		Either "answer" or "topic".
	 * @param userId	The voter user's id.
	 * @param fieldId	The field id to clear votes on.
	 * @throws PlatzException
	 */
	protected void clearVote(String field,
			long userId, long fieldId) throws PlatzException {
		String query = "DELETE FROM " + field + "_votes "
				+ "WHERE " + field + "_id = ? AND user_id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, fieldId);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Add a category.
	 * 
	 * Returns false if the category is not found after inserting it.
	 * 
	 * @param Category	The Category object.
	 * @return			true on success, false on failure
	 * @throws PlatzException 
	 */
	public boolean addCategory(Category category) throws PlatzException {
		String query = "INSERT INTO categories "
				+ "(name) "
				+ "VALUES (?)";
		
		long categoryId = -1;
		
		ResultSet generatedKeys = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, category.getName());
			
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
			
			generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				categoryId = generatedKeys.getLong(1);
	        } else {
	        	throw new PlatzException(ErrorCode.SQL_ERROR,
						"The INSERT query did not affect a single row");
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, generatedKeys);
		}
		
		return getCategoryById(category, categoryId);
	}
	
	/**
	 * Remove a category.
	 * 
	 * @param category		The Category object to remove.
	 * @throws PlatzException 
	 */
	public void removeCategory(Category category) 
			throws PlatzException {
		String query = "DELETE FROM categories "
				+ "WHERE id = ?";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, category.getId());
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	/**
	 * Get one topic by its id.
	 * 
	 * @param topic	The Topic object.
	 * @param id	The topic id.
	 * @return		true on success, false otherwise
	 * @throws PlatzException 
	 */
	public boolean getTopicById(Topic topic, long id) 
			throws PlatzException {
		String query = "SELECT id,title,content,created_on,creator_id,anonymous "
				+ "FROM topics WHERE "
				+ "id = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				topic.setId(id);
				topic.setTitle(rs.getString(2));
				topic.setContent(rs.getString(3));
				topic.setCreatedOn(rs.getTimestamp(4));
				topic.setCreatorId(rs.getLong(5));
				
				return true;
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get one answer by its id.
	 * 
	 * @param answer	The Answer object.
	 * @param id		The answer id.
	 * @return			true on success, false otherwise
	 * @throws PlatzException 
	 */
	public boolean getAnswerById(Answer answer, long id) 
			throws PlatzException {
		String query = "SELECT id,content,created_on,topic_id,creator_id,anonymous "
				+ "FROM answers WHERE "
				+ "id = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				answer.setId(id);
				answer.setContent(rs.getString(2));
				answer.setCreatedOn(rs.getTimestamp(3));
				answer.setTopicId(rs.getLong(4));
				answer.setCreatorId(rs.getLong(5));
				answer.setAnonymous(rs.getBoolean(6));
				
				return true;
			} else {
				return false;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a category by its id.
	 * 
	 * @param category	The Category object.
	 * @param id		The category id.
	 * @return			true on success, false on failure
	 * @throws PlatzException 
	 */
	public boolean getCategoryById(Category category, long id) 
			throws PlatzException {
		String query = "SELECT id,name "
				+ "FROM categories WHERE "
				+ "id = ? "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, id);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				category.setId(id);
				category.setName(rs.getString(2));
				
				return true;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
		return false;
	}
	
	/**
	 * Get a list of visible topics, ordered by creation date.
	 * The list could be empty if no topics are found.
	 * 
	 * @return	List of Topic objects
	 * @throws PlatzException 
	 */
	public Topic[] getTopicsByDate() throws PlatzException {
		String query = "SELECT id,title,content,created_on,creator_id,anonymous "
				+ "FROM topics "
				+ "ORDER BY created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}

	/**
	 * Get a list of visible topics created by a user,
	 * ordered by creation date.
	 * The list could be empty if no topics are found
	 * or if the user is not found.
	 * 
	 * @param creator		The User object of the creator.
	 * @return				List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getTopicsCreatedByUser(User creator) 
			throws PlatzException {
		String query = "SELECT id,title,content,created_on,creator_id,anonymous "
				+ "FROM topics WHERE "
				+ "anonymous = false AND creator_id = ? "
				+ "ORDER BY created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, creator.getId());
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a list of visible topics belonging to a category,
	 * ordered by creation date.
	 * The list could be empty if no topics are found
	 * or if the category is not found.
	 * 
	 * @param category		The Category object.
	 * @return				List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getTopicsByCategory(Category category) 
			throws PlatzException {
		String query = "SELECT t.id,t.title,t.content,t.created_on,t.creator_id,t.anonymous "
				+ "FROM topics t "
				+ "LEFT JOIN topic_categories tc ON tc.topic_id = t.id"
				+ "WHERE "
				+ "tc.category_id = ? "
				+ "ORDER BY t.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
	
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, category.getId());
			
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a list of visible topics belonging to any
	 * of the given categories and matching the search
	 * terms, ordered by creation date.
	 * 
	 * Note: the search is not efficient at all.
	 * 
	 * @param category		The Category object.
	 * @return				List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getTopicsByCategoriesAndSearch(long[] categoryIds, String search) 
			throws PlatzException {
		
		String categoryFilterQuery = null;
		if (categoryIds != null && categoryIds.length > 0) {
			StringBuilder categoryFilter = new StringBuilder("(");
			for (int i = 0; i<categoryIds.length-1; i++) {
				categoryFilter.append(Long.toString(categoryIds[i]) + ",");
			}
			categoryFilter.append(Long.toString(categoryIds[categoryIds.length - 1]));
			categoryFilter.append(")");
			
			categoryFilterQuery = "tc.category_id IN " + categoryFilter.toString();
		}
		
		String query = "SELECT t.id,t.title,t.content,t.created_on,t.creator_id,t.anonymous "
				+ "FROM topics t "
				+ "LEFT JOIN topic_categories tc ON tc.topic_id = t.id "
				+ "WHERE "
				+ (categoryFilterQuery == null? categoryFilterQuery : "1") + " "
				+ "AND t.title LIKE '%' || ? || '%' "
				+ "AND t.content LIKE '%' || ? || '%' "
				+ "ORDER BY t.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			// Categories are added before, unescaped because they are longs
			ps.setString(1, search);
			ps.setString(1, search);
			
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a list of topics in which a user could be interested,
	 * ordered by creation date.
	 * Interesting topics are determined by subscriptions to topics,
	 * and/or subscriptions to categories.
	 * 
	 * The list could be empty if no topics are found,
	 * or if the user is not found.
	 * 
	 * @param user		The User object.
	 * @return			List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getTopicsForUser(User user) 
			throws PlatzException {
		String query = "SELECT t.id,t.title,t.content,t.created_on,t.creator_id,t.anonymous "
				+ "FROM topics t "
				+ "LEFT JOIN topic_categories tc ON tc.topic_id = t.id "
				+ "LEFT JOIN topic_subscriptions ts ON ts.topic_id = t.id "
				+ "LEFT JOIN category_subscriptions cs ON cs.category_id = tc.category_id "
				+ "WHERE "
				+ "cs.user_id = ? AND ts.user_id = ?"
				+ "ORDER BY t.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, user.getId());
			ps.setLong(2, user.getId());
			
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a list of topics to which a user is subscribed,
	 * ordered by creation date.
	 * The list could be empty if no topics are found,
	 * or if the user does not exist, or if there are no
	 * subscriptions to topics.
	 * 
	 * @param user		The User object of the subscriber.
	 * @return			List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getTopicSubscriptions(User user) 
			throws PlatzException {
		String query = "SELECT t.id,t.title,t.content,t.created_on,t.creator_id,t.anonymous "
				+ "FROM topics t "
				+ "LEFT JOIN topic_subscriptions ts ON ts.topic_id = t.id "
				+ "WHERE "
				+ "ts.user_id = ?"
				+ "ORDER BY t.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, user.getId());
			
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get a list of topics to whose category the user is subscribed,
	 * ordered by creation date.
	 * The list could be empty if no topics are found,
	 * or if the user does not exist, or if there are no
	 * subscriptions to categories.
	 * 
	 * @param user		The User object of the subscriber.
	 * @return			List of Topic objects.
	 * @throws PlatzException 
	 */
	public Topic[] getCategorySubscriptions(User user) 
			throws PlatzException {
		String query = "SELECT t.id,t.title,t.content,t.created_on,t.creator_id,t.anonymous "
				+ "FROM topics t "
				+ "LEFT JOIN topic_categories tc ON tc.topic_id = t.id "
				+ "LEFT JOIN category_subscriptions cs ON cs.category_id = tc.category_id "
				+ "WHERE "
				+ "cs.user_id = ?"
				+ "ORDER BY t.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, user.getId());
			
			rs = ps.executeQuery();
			return getTopicListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Fill a list with objects based on the results of a query.
	 * 
	 * @param rs	The result set of the executed query.
	 * @param list	The list to fill.
	 * @throws SQLException
	 */
	protected Topic[] getTopicListFromResultSet(ResultSet rs) 
			throws SQLException {
		ArrayList<Topic> list = new ArrayList<Topic>();
		while (rs.next()) {
			Topic t = new Topic();
			t.setId(rs.getLong(1));
			t.setTitle(rs.getString(2));
			t.setContent(rs.getString(3));
			t.setCreatedOn(rs.getTimestamp(4));
			t.setCreatorId(rs.getLong(5));
			
			list.add(t);
		}
		return (Topic[]) list.toArray();
	}
	
	/**
	 * Get the visible answers of a certain topic, ordered by creation date.
	 * The list could be empty if there are no visible answers,
	 * or the topic is hidden.
	 * 
	 * @param topic		The Topic object.
	 * @return			List of Answer objects.
	 * @throws PlatzException 
	 */
	public Answer[] getAnswersByTopic(Topic topic) 
			throws PlatzException {
		String query = "SELECT a.id,a.content,a.created_on,a.topic_id,a.creator_id,a.anonymous "
				+ "FROM answers a "
				+ "LEFT JOIN topics t ON t.id = a.topic_id "
				+ "WHERE "
				+ "a.topic_id = ?"
				+ "ORDER BY a.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
	
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, topic.getId());
			
			rs = ps.executeQuery();
			return getAnswerListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get the visible answers of a user, ordered by creation date.
	 * 
	 * @param creator		The User object of the creator.
	 * @return				List of Answer objects.
	 * @throws PlatzException 
	 */
	public Answer[] getAnswersCreatedByUser(User creator) 
			throws PlatzException {
		String query = "SELECT a.id,a.content,a.created_on,a.topic_id,a.creator_id,a.anonymous "
				+ "FROM answers a "
				+ "LEFT JOIN topics t ON t.id = a.topic_id "
				+ "WHERE "
				+ "a.shown AND a.creator_id = ?"
				+ "ORDER BY a.created_on DESC ";
				//+ "LIMIT " + offset + "," + limit;
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, creator.getId());
			
			rs = ps.executeQuery();
			return getAnswerListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Fill a list with objects based on the results of a query.
	 * 
	 * @param rs	The result set of the executed query.
	 * @param list	The list to fill.
	 * @throws SQLException
	 */
	protected Answer[] getAnswerListFromResultSet(ResultSet rs) 
			throws SQLException {
		ArrayList<Answer> list = new ArrayList<Answer>();
		while (rs.next()) {
			Answer a = new Answer();
			a.setId(rs.getLong(1));
			a.setContent(rs.getString(2));
			a.setCreatedOn(rs.getTimestamp(3));
			a.setTopicId(rs.getLong(4));
			a.setCreatorId(rs.getLong(5));
			a.setAnonymous(rs.getBoolean(6));
			
			list.add(a);
		}
		return (Answer[]) list.toArray();
	}
	
	/**
	 * Get a list of categories, sorted alphabetically.
	 * 
	 * @return	List of Category objects.
	 * @throws PlatzException 
	 */
	public Category[] getCategories() throws PlatzException {
		String query = "SELECT id,name "
				+ "FROM categories "
				+ "ORDER BY name ASC ";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			rs = ps.executeQuery();
			return getCategoryListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Get the categories to which a topic belongs.
	 * 
	 * @param topic		The Topic object.
	 * @return			List of Category objects.
	 * @throws PlatzException 
	 */
	public Category[] getTopicCategories(Topic topic) 
			throws PlatzException {
		String query = "SELECT c.id,c.name "
				+ "FROM categories c "
				+ "LEFT JOIN topic_categories tc ON tc.category_id = c.id "
				+ "WHERE tc.topic_id = ? "
				+ "ORDER BY c.name ASC ";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, topic.getId());
			
			rs = ps.executeQuery();
			return getCategoryListFromResultSet(rs);
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	/**
	 * Fill a list with objects based on the results of a query.
	 * 
	 * @param rs	The result set of the executed query.
	 * @param list	The list to fill.
	 * @throws SQLException
	 */
	protected Category[] getCategoryListFromResultSet(ResultSet rs)
			throws SQLException {
		ArrayList<Category> list = new ArrayList<Category>();
		while (rs.next()) {
			Category c = new Category();
			c.setId(rs.getLong(1));
			c.setName(rs.getString(2));
			
			list.add(c);
		}
		return (Category[]) list.toArray();
	}
}
