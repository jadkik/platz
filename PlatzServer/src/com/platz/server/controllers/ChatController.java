package com.platz.server.controllers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nu.xom.*;

import com.platz.server.RequestReader;
import com.platz.server.ResponseWriter;
import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.User;
import com.platz.util.ErrorCode;

public class ChatController extends Controller {
	public ChatController(CoreController core) {
		super(core);
	}
	
	public void setPresence(long userId, String activeLoginCode, String ipAddress, int port)
			throws PlatzException {
		int affectedRows = updatePresence(userId, activeLoginCode, ipAddress, port);
		if (affectedRows == 0) {
			insertPresence(userId, activeLoginCode, ipAddress, port);
		}
	}
	
	public String[] getOnlineUsernames(User forUser)
			throws PlatzException {
			
			// Time before a user is considered offline (in seconds)
			int timeoutInSeconds = 5 * 60;
			
			String query = "SELECT u.username "
					+ "FROM chat_presence cp "
					+ "LEFT JOIN users u ON u.id = cp.user_id "
					+ "WHERE cp.last_updated >= NOW() - " + timeoutInSeconds + " "
					+ "AND u.id != ? "
					+ "ORDER BY u.username ASC";
			
			ArrayList<String> onlineUsernames = new ArrayList<String>();
			
			ResultSet rs = null;
			PreparedStatement ps = null;
			try {
				ps = db.prepareStatement(query);
				ps.setLong(1, forUser.getId());
				
				rs = ps.executeQuery();
				while (rs.next()) {
					onlineUsernames.add(rs.getString(1));
				}
			} catch (SQLException ex) {
				throw new PlatzException(ErrorCode.SQL_ERROR,
						"SQL Error occured", ex);
			} finally {
				db.closeStatement(ps, rs);
			}
			
			String[] usernames = new String[onlineUsernames.size()];
			
			return onlineUsernames.toArray(usernames);
		}
	
	public Presence[] getPresence(String username)
			throws PlatzException {
		
		// Time before a user is considered offline (in seconds)
		int timeoutInSeconds = 5 * 60;
		
		String query = "SELECT cp.user_id,cp.active_login_code,cp.chat_ip,cp.chat_port,cp.last_updated "
				+ "FROM chat_presence cp "
				+ "LEFT JOIN users u ON u.id = cp.user_id "
				+ "WHERE u.username = ? AND cp.last_updated >= NOW() - " + timeoutInSeconds + " "
				+ "ORDER BY cp.last_updated DESC";
		
		ArrayList<Presence> chatAddresses = new ArrayList<Presence>();
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, username);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				Presence p = new Presence();
				p.ipAddress = rs.getString(3);
				p.port = rs.getInt(4);
				chatAddresses.add(p);
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
		
		Presence[] presences = new Presence[chatAddresses.size()];
		
		return chatAddresses.toArray(presences);
	}
	
	private void insertPresence(long userId, String activeLoginCode, String ipAddress, int port)
			throws PlatzException {
		String query = "INSERT IGNORE INTO chat_presence "
				+ "(user_id, active_login_code, chat_ip, chat_port) "
				+ "VALUES (?, ?, ?, ?)";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setLong(1, userId);
			ps.setString(2, activeLoginCode);
			ps.setString(3, ipAddress);
			ps.setInt(4, port);
			
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}
	
	private int updatePresence(long userId, String activeLoginCode, String ipAddress, int port)
			throws PlatzException {
		String query = "UPDATE chat_presence "
				+ "SET chat_ip = ?, chat_port = ?, last_updated = NOW() "
				+ "WHERE user_id = ? AND active_login_code = ? "
				+ "LIMIT 1";
		
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, ipAddress);
			ps.setInt(2, port);
			ps.setLong(3, userId);
			ps.setString(4, activeLoginCode);
			
			int affectedRows = ps.executeUpdate();
			return affectedRows;
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps);
		}
	}

	public String getUsername(String ipAddress, int port)
			throws PlatzException {
		
		// Time before a user is considered offline (in seconds)
		int timeoutInSeconds = 5 * 60;
		
		String query = "SELECT cp.user_id,u.username,cp.chat_ip,cp.chat_port,cp.last_updated "
				+ "FROM chat_presence cp "
				+ "LEFT JOIN users u ON u.id = cp.user_id "
				+ "WHERE cp.chat_ip = ? AND cp.chat_port = ? AND cp.last_updated >= NOW() - " + timeoutInSeconds + " "
				+ "ORDER BY cp.last_updated DESC "
				+ "LIMIT 1";
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			ps = db.prepareStatement(query);
			ps.setString(1, ipAddress);
			ps.setInt(2, port);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(2);
			} else {
				return null;
			}
		} catch (SQLException ex) {
			throw new PlatzException(ErrorCode.SQL_ERROR,
					"SQL Error occured", ex);
		} finally {
			db.closeStatement(ps, rs);
		}
	}
	
	public static class Presence {
		public String ipAddress;
		public int port;
	}
	
	//-----------------------------------------------------=================--------------------------
	
	/*
	 <request action= "getusernamebyipandport">
			<ip></ip>
			<port></port>
	 </request>
	 */
	
	public static Presence readUserNameByIPandPortRequest(String xmlString) throws PlatzException{
		Element root = RequestReader.getRootElement(xmlString);
		Presence result = new Presence();
		result.ipAddress = root.getChild(0).getValue();
		result.port = Integer.valueOf(root.getChild(1).getValue());
		
		return result;
	}
		
	
	/*
	 <response success = "true">
	 	<username></username>
	 </response>
	 */
	public static String writeUserNameByIPandPortResponse(String userNameArg){
		Element root = new Element ("response");
		root.addAttribute(new Attribute("success","true"));
		
		Element userName = new Element("username");
		userName.appendChild(userNameArg);
		
		root.appendChild(userName);
		
		return ResponseWriter.format(root);

	}
	
	
	
	//----
	
	
	// no need for a read this request since it has no elements
	/*
	 <request action= "getonlineusers">
	 </request>
	 */
	
	//----
	
	
	
	
	/*
	 <response success = "true">
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 	<username></username>
	 </response>
	 */
	
	public static String writeListOfOnlineUsersResponse(String[] onlineUserNames){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success","true"));
		
		for(int i=0; i<onlineUserNames.length; i++){
			Element userName = new Element("username");
			userName.appendChild(onlineUserNames[i]);
			root.appendChild(userName);
		}
		
		return ResponseWriter.format(root);
	}
	
	
	/*
	 <request action= "getipandportbyusername">
	 <username></username>
	 </request>
	 */
	
	
	public static String readIPandPortByUserNameRequest(String xmlString) throws PlatzException{
		Element root = RequestReader.getRootElement(xmlString);
		String result = root.getChild(0).getValue();

		return result;
	}
	
	/*
	 <response success = "true">
	 	<ip></ip>
	 	<port></port>
	 </response>
	 */

	public static String writeIPandPortByUserNameResponse(Presence id){
		Element root = new Element ("response");
		root.addAttribute(new Attribute("success","true"));
		
		Element IP = new Element("ip");
		Element port = new Element("port");
		
		IP.appendChild(id.ipAddress);
		port.appendChild(Integer.toString(id.port));
		
		root.appendChild(IP);
		root.appendChild(port);
		
		return ResponseWriter.format(root);
		
	}
	
	
	
	
}
