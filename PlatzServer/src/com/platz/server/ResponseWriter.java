package com.platz.server;

import java.text.SimpleDateFormat;

import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;

import com.platz.server.models.User;
import com.platz.server.models.UserProfile;
import com.platz.server.models.ActiveLogin;
import com.platz.server.models.Category;
import com.platz.server.exceptions.PlatzException;
import com.platz.server.models.Topic;

public class ResponseWriter {

	static public String format(Element root) {
		Document doc = new Document(root);
		return doc.toXML();
	}

	static public String writeErrorResponse(PlatzException ex) {
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "false"));
		Element error = new Element("error");

		error.addAttribute(new Attribute("code", Integer.toString(ex.getCode())));
		error.appendChild(ex.getMessage());

		root.appendChild(error);
		return format(root);
	}
	
	static public String writeEmptySuccessResponse() {
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));
		return format(root);
	}


	/*
	 <response success = "true">
	 	<id></id>
	 </response>
	 */


	static public String writePostTopicResponse(Topic topic) {
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		Element id = new Element("id");

		id.appendChild(Long.toString(topic.getId()));

		root.appendChild(id);
		return format(root);
	}



	/*
	 <response success = "true">
	 	<id></id>
	 	<title></title>
		<content></content>
		<anonymous></anonymous>
		<creatorid></creatorid>
		<createdOn></createdOn>
		<categories>
			<category></category>
			<category></category>
			.
			.
			.
		</categories>
	</response>
	 */

	public static String writeGetTopicResponse(Topic topic){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		Element id = new Element("id");
		Element title = new Element("title");
		Element content = new Element("content");
		Element anonymous = new Element("anonymous");
		Element creatorId = new Element("creatorid");
		Element createdOn = new Element("createdon");
		Element categories = new Element("categories");

		id.appendChild(Long.toString(topic.getId()));
		title.appendChild(topic.getTitle());
		content.appendChild(topic.getContent());
		anonymous.appendChild(Boolean.toString(topic.isAnonymous()));
		creatorId.appendChild(Long.toString(topic.getCreatorId()));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

		createdOn.appendChild(sdf.format(topic.getCreatedOn()));

		long categoriesArray[] = topic.getCategoires();
		for(int i=0; i<categoriesArray.length; i++){
			Element category = new Element("category");
			category.appendChild(Long.toString(categoriesArray[i]));
			categories.appendChild(category);
		}

		root.appendChild(id);
		root.appendChild(title);
		root.appendChild(content);
		root.appendChild(anonymous);
		root.appendChild(creatorId);
		root.appendChild(createdOn);
		root.appendChild(categories);

		return format(root);
	}

	/*
	 <response success = "true">
			<topic> 	
				<id></id>
	 			<title></title>
				<createdOn></createdOn>
			</topic>
			.
			.
			.
	</response>
	 */
	//--
	public static String writeGetListTopicsResponse(Topic[] topics){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		for(int i=0; i<topics.length; i++){
			Element topic = new Element("topic");

			Element id = new Element("id");
			Element title = new Element("title");
			Element createdOn = new Element("createdOn");

			id.appendChild(Long.toString(topics[i].getId()));
			title.appendChild(topics[i].getTitle());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");



			createdOn.appendChild(sdf.format(topics[i].getCreatedOn()));

			topic.appendChild(id);
			topic.appendChild(title);
			topic.appendChild(createdOn);

			root.appendChild(topic);
		}

		return format(root);
	}


	/*
	 <response success = "true">
	 	<category>
			<name></name>
			<id></id>
		</category>
			.
			.
			.
	</response>
	 */



	public static String writeListCategoriesResponse(Category[] categories){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		for(int i=0; i<categories.length; i++){
			Element category = new Element("category");

			Element name = new Element("name");
			Element id = new Element ("id");

			name.appendChild(categories[i].getName());
			id.appendChild(Long.toString(categories[i].getId()));

			category.appendChild(name);
			category.appendChild(id);

			root.appendChild(category);
		}

		return format(root);
	}



	/*
	<response success = "true">
			<id></id>
			<code></code>
			<createdon></createdon>
			<expireson></expiresOn>
	</response>
	 */
	public static String writeLoginResponse(ActiveLogin activeLogin){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		Element id = new Element("id");
		Element code = new Element("code");
		Element createdOn = new Element("createdon");
		Element expiresOn = new Element("expireson");

		id.appendChild(Long.toString(activeLogin.getUserId()));
		code.appendChild(activeLogin.getCode());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		createdOn.appendChild(sdf.format(activeLogin.getCreatedOn()));
		expiresOn.appendChild(sdf.format(activeLogin.getExpiresOn()));

		root.appendChild(id);
		root.appendChild(code);
		root.appendChild(createdOn);
		root.appendChild(expiresOn);

		return format(root);

	}
	/*
	<response success = "true">
	</response>
	 */
	public static String writeSignUpResponse(){
		return writeEmptySuccessResponse();
	}
	
	/*
	 <request success = "true">
			<firstname></firstname>
			<lastname></lastname>
			<profiletext></profiletext>
			<college></college>
			<birthday></birhday>
			<interests></interests>
	 </request>
	 */
	public static String writeEditProfileResponse(User user){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));

		
		Element firstName = new Element("firstname");
		Element lastName = new Element("lastname");
		Element profileText = new Element("profiletext");
		Element college = new Element("college");
		Element birthday = new Element("birthday");
		Element interests = new Element("interests");
		
		firstName.appendChild(user.getFirstName());
		lastName.appendChild(user.getLastName());
		profileText.appendChild(user.getProfileText());
		college.appendChild(user.getCollege());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		birthday.appendChild(sdf.format(user.getBirthday()));
		
		interests.appendChild(user.getInterests());
		
		root.appendChild(firstName);
		root.appendChild(lastName);
		root.appendChild(profileText);
		root.appendChild(college);
		root.appendChild(birthday);
		root.appendChild(interests);
		
		return format(root);
	}

	/*
	 <request success = "true">
	 	<username></username>
	 	<email></email>
	 	<first></firstname>
	 	<lastname></lastname>
	 	<profiletext></profiletext>
	 	<birthday></birthday>
	 	<createdon></createdon>
	 	<latestanswers>
	 		<answer>
	 			<id></id>
	 			<partofcontent></date>
	 			<date></date>
	 		</answer>
	 		.
	 		.
	 		.
	 	</latestanswers>
	 	<latestquestions>
	 		<question>
	 			<id></id>
	 			<title></title>
	 			<date></date>
	 		</question>
	 		.
	 		.
	 		.
	 	</latestquestions>
	 </request>
	 */
	public static String writeUserInfoResponse(UserProfile profile){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));
		
		Element userName = new Element("username");
		Element email = new Element("email");
		Element firstName = new Element("firstname");
		Element lastName = new Element("lastname");
		Element profileText = new Element("profiletext");
		Element birthday = new Element("birthday");
		Element createdOn = new Element("createdon");
		Element latestAnswers = new Element("lastanswers");
		Element latestQuestions = new Element("lastquestions");
		
		userName.appendChild(profile.getUsername());
		email.appendChild(profile.getEmail());
		firstName.appendChild(profile.getFirstName());
		lastName.appendChild(profile.getLastName());
		profileText.appendChild(profile.getProfileText());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		birthday.appendChild(sdf.format(profile.getBirthday()));
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		createdOn.appendChild(sdf1.format(profile.getCreatedOn()));
		
		
		for(int i=0; i<profile.getAnswers().length; i++){
			Element answer = new Element("answer");
			
			Element id = new Element("id");
			Element partOfContent = new Element("partofcontent");
			Element date = new Element ("date");
			
			id.appendChild(Long.toString(profile.getAnswers()[i].getId()));
			partOfContent.appendChild(profile.getAnswers()[i].getPartOfContent());
			date.appendChild(sdf1.format(profile.getAnswers()[i].getCreatedOn()));
			
			answer.appendChild(id);
			answer.appendChild(partOfContent);
			answer.appendChild(date);
			
			latestAnswers.appendChild(answer);
		}
		
		for(int i=0; i<profile.getQuestions().length; i++){
			Element question = new Element("question");
			
			Element id = new Element("id");
			Element title = new Element("title");
			Element date = new Element ("date");
			
			id.appendChild(Long.toString(profile.getQuestions()[i].getId()));
			title.appendChild(profile.getQuestions()[i].getTitle());
			date.appendChild(sdf1.format(profile.getQuestions()[i].getCreatedOn()));
			
			latestQuestions.appendChild(question);
		}
		
		root.appendChild(userName);
		root.appendChild(email);
		root.appendChild(firstName);
		root.appendChild(lastName);
		root.appendChild(profileText);
		root.appendChild(birthday);
		root.appendChild(createdOn);
		root.appendChild(latestAnswers);
		root.appendChild(latestQuestions);
		
		return format(root);
	}
	
/*
	<response success = "true">
	</response>
 */
	
	public static String writeSubscriptionResponse(){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));
	
		return format(root);
	}
	
	
/*
	<response success = "true">
	</response>
*/
	
	public static String writeAnswerResponse(){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));
		
		return format(root);
	}
	
	/*
	<response success = "true">
	</response>
 */
	
	public static String writeUnSubscriptionResponse(){
		Element root = new Element("response");
		root.addAttribute(new Attribute("success", "true"));
	
		return format(root);
	}
	
	
	
	
}
