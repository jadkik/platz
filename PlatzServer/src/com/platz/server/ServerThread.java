package com.platz.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.platz.server.controllers.CoreController;
import com.platz.server.exceptions.PlatzClientException;
import com.platz.server.exceptions.PlatzException;

class ServerThread implements Runnable {

	private Socket socket;
	private BufferedReader datain;
	private DataOutputStream dataout;

	private CoreController ctrl;

	public ServerThread(Socket socket, CoreController ctrl) {
		this.socket = socket;
		this.ctrl = ctrl;
	}

	public void run() {
		// Create the readers/writers
		try {
			datain =  new BufferedReader(new InputStreamReader(socket.getInputStream()));
			dataout = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			return;
		}

		// Update the ServerController
		ctrl.server.setDataOut(dataout);
		ctrl.server.setDataIn(datain);

		// Keep processing requests
		try {
			while (true) {
				String requestXml;
				try {
					requestXml = read();
					
					System.out.println(requestXml);
					
					if (!ctrl.server.handleRequest(requestXml)) {
						return;
					}
				} catch (NumberFormatException ex) {
					// This is because either the connection was interrupted
					// (length is null)
					// Or something other than an integer was sent on a single line
					// So it failed
					// TODO log it and ignore
					continue;
				} catch (PlatzClientException ex) {
					// This is an exception that should be sent to the client
					try {
						ctrl.server.sendErrorResponse(ex);
					} catch (PlatzException e) {
						// TODO log the error
						e.printStackTrace();
					}
				} catch (PlatzException ex) {
					// Some other internal error
					// We don't send details of the error to the client
					// Just say "server error"
					try {
						ctrl.server.sendServerErrorResponse();
					} catch (PlatzException e) {
						// We can't even tell the client there was a server error
						// TODO log the error
						e.printStackTrace();
					}
					// TODO log the error
					ex.printStackTrace();
				}
			}
		} catch (IOException ex) {
			// IOException probably means connection is lost
			// So we stop waiting for requests
			// TODO log the error
			ex.printStackTrace();
		} finally {
			// Always close the connection after all requests have been handled
			try {
				System.out.println("closing socket");
				datain.close();
				dataout.close();
				socket.close();
			} catch (IOException e) {
				// Do nothing.
			}
		}
	}
	
	protected String read() throws IOException {
		String line = datain.readLine();
		int len = Integer.valueOf(line);
		return readLength(len);
	}
	
	protected String readLength(int len) throws IOException {
		final int AT_ONCE = 1024;

    	char[] buf = new char[AT_ONCE];
        int numRead = 0;
        int numRemaining = len;
        int maxRead = Math.min(numRemaining, AT_ONCE);
        
        StringBuilder requestData = new StringBuilder();
        
        while((numRead = datain.read(buf, 0, maxRead)) != -1) {
		    String readData = String.valueOf(buf, 0, numRead);
		    requestData.append(readData);
		    
		    buf = new char[AT_ONCE];
		    numRemaining -= numRead;
		    maxRead = Math.min(numRemaining, AT_ONCE);
		    
		    if (numRemaining == 0) {
		    	break;
		    }
		}
		
		return requestData.toString();
	}
}
