package com.platz.server;

import java.io.*;
import java.net.*;

import com.platz.server.controllers.CoreController;
import com.platz.server.database.DatabaseConfig;

public class Server {

    private int port = 1927;
    private ServerSocket serverSocket;

    private ServerConfig config;
    private DatabaseConfig dbconfig;
    
    public Server(ServerConfig config, DatabaseConfig dbconfig) {
    	this.config = config;
    	this.dbconfig = dbconfig;
    }

    /**
     * Start the server and listen for client connections.
     */
    public void acceptConnections() {
    	// Create the server socket
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
        	// TODO log the error and handle it in Main, not here
            System.err.println("ServerSocket instantiation failure");
            e.printStackTrace();
            System.exit(0);
        }

        // Listen for connections
        while (true) {
            try {
            	// Wait for a connection. This call is blocking.
                Socket newConnection = serverSocket.accept();
                
                // One controller for each client
                CoreController ctrl = new CoreController(config, dbconfig);
                
                // Create a thread for each client
                ServerThread st = new ServerThread(newConnection, ctrl);
                new Thread(st).start();
            } catch (IOException ex) {
            	// Ignore the error
                System.err.println("server accept failed");
            }
        }
    }
}
