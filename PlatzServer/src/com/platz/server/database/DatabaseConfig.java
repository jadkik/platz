package com.platz.server.database;

import java.util.Properties;

import com.platz.server.ServerConfig;
import com.platz.server.exceptions.PlatzException;
import com.platz.util.ErrorCode;

/**
 * This class holds the database configuration details
 * necessary to connect to a database.
 * 
 * @author jkik
 *
 */
public class DatabaseConfig {
	
	private String hostname;
	private int port;
	private String username;
	private String password;
	private String database;
	
	public static final String DEFAULT_HOSTNAME = "localhost";
	public static final int DEFAULT_PORT = 3306;
	public static final String DEFAULT_USERNAME = "root";
	public static final String DEFAULT_PASSWORD = null;
	public static final String DEFAULT_DATABASE = "platz";
	
	/**
	 * Default configuration.
	 */
	public DatabaseConfig() {
		setHostname(DEFAULT_HOSTNAME);
		setPort(DEFAULT_PORT);
		setDatabase(DEFAULT_DATABASE);
		setUsername(DEFAULT_USERNAME);
		setPassword(DEFAULT_PASSWORD);
	}
	
	/**
	 * Custom configuration.
	 * 
	 * @param hostname	The hostname of the MySQL server.
	 * @param port		The port number of the MySQL server.
	 * @param database	The name of the database to use.
	 * @param username	The username to use to connect.
	 * @param password	The password of the given username.
	 */
	public DatabaseConfig(String hostname, int port, String database, String username, String password) {
		setHostname(hostname);
		setPort(port);
		setDatabase(database);
		setUsername(username);
		setPassword(password);
	}
	
	/**
	 * Basic configuration.
	 * 
	 * @param hostname	The hostname of the MySQL server.
	 * @param database	The name of the database to use.
	 * @param username	The username to use to connect.
	 * @param password	The password of the given username.
	 */
	public DatabaseConfig(String hostname, String database, String username, String password) {
		setHostname(hostname);
		setPort(DEFAULT_PORT);
		setDatabase(database);
		setUsername(username);
		setPassword(password);
	}
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * Builds a URL suitable to use with a JDBC connector.
	 * 
	 * @return	The JDBC URL to use for jdbc-mysql-connector
	 */
	public String getJdbcUrl() {
		return "jdbc:mysql://" +
				hostname +
				":" + port +
				"/" + getDatabase() +
				"?zeroDateTimeBehavior=convertToNull";
	}
	
	/**
	 * Creates a DatabaseConfig object from a coniguration file.
	 * 
	 * @param filename	The filename of the configuration file.
	 * @return			A DatabaseConfig object
	 * @throws PlatzException 
	 */
	static public DatabaseConfig fromConfig(ServerConfig config) throws PlatzException {
		DatabaseConfig dbconfig = new DatabaseConfig();
		
		try {
			Properties props = config.getProperties();
			
			dbconfig.setHostname(props.getProperty("database.hostname", DEFAULT_HOSTNAME));
			
			String port = props.getProperty("database.port");
			dbconfig.setPort(port == null? DEFAULT_PORT : Integer.valueOf(port));
			
			dbconfig.setUsername(props.getProperty("database.username", DEFAULT_USERNAME));
			dbconfig.setPassword(props.getProperty("database.password", DEFAULT_PASSWORD));
			
			dbconfig.setDatabase(props.getProperty("database.name", DEFAULT_DATABASE));
		} catch (NumberFormatException ex) {
			throw new PlatzException(ErrorCode.CONFIG_ERROR,
					"Invalid database port number.", ex);
		} catch (Exception ex) {
			throw new PlatzException(ErrorCode.CONFIG_ERROR, ex);
		}
		
		return dbconfig;
	}
}
