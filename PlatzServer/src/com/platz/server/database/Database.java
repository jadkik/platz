package com.platz.server.database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.platz.server.exceptions.PlatzException;
import com.platz.util.ErrorCode;

public class Database {

	protected Connection conn;
	protected boolean connected = false;
	
	public Database(DatabaseConfig config) {
		connected = connect(config);
	}
	
	/**
	 * Establsihes a connection to the MySQL server
	 * using the given configuration.
	 * 
	 * @param config	The DatabaseConfig object to use.
	 * @return			true on success, false on failure
	 */
	public boolean connect(DatabaseConfig config) {
        try {
            // load mysql driver
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(
            		config.getJdbcUrl(),
            		config.getUsername(),
            		config.getPassword());
            
            System.out.println("Database connection established");
        } catch (SQLException ex) {
        	error(ex);
            return false;
        } catch (ClassNotFoundException ex) {
        	Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (InstantiationException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
	}
	
	/**
	 * Returns true if the connection was established
	 * and was not closed yet.
	 * 
	 * @return	true if connected, false otherwise
	 */
	public boolean isConnected() {
		return connected && conn != null;
	}
	
	/**
	 * Generate a PreparedStatement using the given SQL query.
	 * 
	 * If not connected, returns null.
	 * 
	 * @param sql	The SQL query.
	 * @return		The PreparedStatement or null on failure
	 * @throws SQLException
	 * @throws PlatzException 
	 */
	public PreparedStatement prepareStatement(String sql) 
			throws SQLException, PlatzException {
		if (!isConnected()) {
			throw new PlatzException(ErrorCode.DB_ERROR, "Cannot query if not connected.");
		}
		
		return conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
	}
	
	/**
	 * Close statement and result sets.
	 * Should be called in the finally block of
	 * a query execution.
	 * 
	 * @param ps	The prepared statement.
	 * @param rs	The result set.
	 */
	public void closeStatement(PreparedStatement ps, ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				error(ex);
			}
		}
		
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException ex) {
				error(ex);
			}
		}
	}
	
	/**
	 * Close statement only (if no result set is used).
	 * Should be called in the finally block of
	 * a query execution.
	 * 
	 * @param ps	The prepared statement.
	 */
	public void closeStatement(PreparedStatement ps) {
		closeStatement(ps, null);
	}
	
	/**
	 * Close the connection to the database.
	 * 
	 */
	public void close() {
		if (!isConnected()) {
			// If not connected do nothing
			return;
		}
		
		try {
            conn.close();
        } catch (SQLException ex) {
        	error(ex);
        }
	}
	
	/**
	 * Log an SQLException which occurs.
	 * Should be called in the catch block of
	 * a query execution.
	 * 
	 * @param ex	The SQLException that occured.
	 */
	public void error(SQLException ex) {
		ex.printStackTrace(System.err);
	}
}
