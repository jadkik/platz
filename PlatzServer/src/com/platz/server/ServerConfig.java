package com.platz.server;

import java.util.*;
import java.io.*;
import java.nio.file.*;

import com.platz.server.exceptions.PlatzException;
import com.platz.util.ErrorCode;

public class ServerConfig {

	private Properties props;
	private String filename;
	
	public ServerConfig(String filename) throws PlatzException {
		props = new Properties();
		setFilename(filename);
	}

	/**
	 * Allows the config to be refreshed at runtime, instead of
	 * requiring a restart.
	 * @throws PlatzException 
	 */
	public void refreshConfig() throws PlatzException {
		getProperties().clear();
		fetchConfig();
	}

	/**
	 * Open a specific text file containing configuration
	 * parameters, and populate a corresponding Properties object.
	 * @throws PlatzException 
	 */
	private void fetchConfig() throws PlatzException {
		//This file contains the javax.mail config properties mentioned above.
		Path path = Paths.get(filename);
		try (InputStream input = Files.newInputStream(path)) {
			getProperties().load(input);
		}
		catch (IOException ex){
			throw new PlatzException(ErrorCode.CONFIG_ERROR,
					"Could not read config file.", ex);
		}
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) throws PlatzException {
		this.filename = filename;
		refreshConfig();
	}

	public Properties getProperties() {
		return props;
	}
}
