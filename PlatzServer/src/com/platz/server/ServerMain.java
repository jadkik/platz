package com.platz.server;

import com.platz.server.database.DatabaseConfig;
import com.platz.server.exceptions.PlatzException;

public class ServerMain {
	
	public static class ChatServerThread implements Runnable {
		ServerConfig config;
		DatabaseConfig dbconfig;
		
		ChatServerThread(ServerConfig config, DatabaseConfig dbconfig) {
			this.config = config;
			this.dbconfig = dbconfig;
		}

		@Override
		public void run() {
			ChatServer s = new ChatServer(config, dbconfig);
			s.connect();
			s.listen();
		}
	}
	
	public static void main(String[] args) throws PlatzException {
		String configFilename = "config/server.txt";
		
		ServerConfig config = new ServerConfig(configFilename);
		DatabaseConfig dbconfig = DatabaseConfig.fromConfig(config);
		
		new Thread(new ChatServerThread(config, dbconfig)).start();
		
		Server server = new Server(config, dbconfig);
        
        server.acceptConnections();
	}

}
