package com.platz.util;

public class ErrorCode {
	
	public static final int UNKNOWN_ERROR = 0x0000;

	public static final int SERVER_INTERNAL_ERROR = 0x0100;
	public static final int SERVER_CLIENT_ERROR = 0x0200;
	public static final int CLIENT_INTERNAL_ERROR = 0x1000;
	
	/**
	 * Server-Side Internal Error Codes
	 * 
	 * These are the ones that should be logged,
	 * but not sent to the client.
	 * 
	 * These start with "0x01--".
	 */
	public static final int DB_ERROR = 0x0110;
	public static final int SQL_ERROR = 0x0111;
	
	public static final int CONFIG_ERROR = 0x0120;
	
	public static final int EMAIL_ERROR = 0x0130;
	
	public static final int REQUEST_XML_ERROR = 0x0140;
	
	/**
	 * Server-Side Client Error Codes
	 * 
	 * These are the ones which the client will be notified of.
	 * 
	 * These start with "0x02--".
	 */
	public static final int USER_ERROR = 0x0220;
	
	public static final int USER_CONFIRM_ERROR = 0x0221;
	public static final int USER_AUTH_ERROR = 0x0222;
	
	/**
	 * Client-Side Error Codes
	 * 
	 * These are for errors which happen on the client,
	 * and should be dealt with on the client.
	 * 
	 * These start with "0x10--".
	 */
	public static final int DATE_FORMAT_ERROR = 0x1010;
	
	public static final int CONNECTION_ERROR = 0x1020;
	
	public static final int RESPONSE_XML_ERROR = 0x1040;
}
