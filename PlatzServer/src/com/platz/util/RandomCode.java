package com.platz.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class RandomCode {
	
	// Check http://www.javapractices.com/topic/TopicAction.do?Id=56
	/**
	 * The byte[] returned by MessageDigest does not have a nice
	 * textual representation, so some form of encoding is usually performed.
	 *
	 * This implementation follows the example of David Flanagan's book
	 * "Java In A Nutshell", and converts a byte array into a String
	 * of hex characters.
	 *
	 * Another popular alternative is to use a "Base64" encoding.
	 */
	static private String hexEncode(byte[] aInput){
		StringBuilder result = new StringBuilder();
		char[] digits = {'0', '1', '2', '3', '4','5','6','7','8','9','a','b','c','d','e','f'};
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[ (b&0xf0) >> 4 ]);
			result.append(digits[ b&0x0f]);
		}
		return result.toString();
	}

	static SecureRandom prng;
	public static String generate(String salt) throws NoSuchAlgorithmException {
		if (prng == null) {
			prng = SecureRandom.getInstance("SHA1PRNG");
		}
		//generate a random number
		String randomNum = new Integer(prng.nextInt()).toString();
		String toHash = salt + "-" + randomNum;

		//get its digest
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		byte[] result =  sha.digest(toHash.getBytes());
		
		//return UUID.randomUUID().toString();
		
		return hexEncode(result);
	}

}
