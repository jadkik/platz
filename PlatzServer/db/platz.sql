
DROP DATABASE IF EXISTS `platz`;
CREATE DATABASE `platz`
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_unicode_ci;

USE `platz`;

/**

User System

****/
CREATE TABLE users (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL,
	
	email VARCHAR(255) DEFAULT NULL,
	first_name VARCHAR(255) DEFAULT NULL,
	last_name VARCHAR(255) DEFAULT NULL,
	profile_text BLOB DEFAULT NULL,
	
	college VARCHAR(255) DEFAULT NULL,
	birthday DATE DEFAULT NULL,
	interests VARCHAR(255) DEFAULT NULL,
	
	active BOOLEAN DEFAULT FALSE,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	last_login TIMESTAMP,
	
	PRIMARY KEY(id)
);

CREATE TABLE active_logins (
	code VARCHAR(255) NOT NULL,
	user_id INT(11) UNSIGNED NOT NULL,
	
	remember INT(1) DEFAULT FALSE,
	
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	expires_on TIMESTAMP DEFAULT 0,
	
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY(code, user_id)
);

CREATE INDEX active_logins_expires_on
	ON active_logins (expires_on DESC);

CREATE TABLE user_confirmations (
	user_id INT(11) UNSIGNED NOT NULL,
	code VARCHAR(8) NOT NULL,
	
	requested_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	expires_on TIMESTAMP DEFAULT 0,
	
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY(user_id, code)
);

/**

Topics and Answers

****/
CREATE TABLE topics (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	content BLOB NOT NULL,
	
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	
	creator_id INT(11) UNSIGNED NOT NULL,
	
	anonymous BOOLEAN DEFAULT FALSE,
	
	FOREIGN KEY (creator_id) REFERENCES users(id),
	PRIMARY KEY(id)
);

CREATE TABLE categories (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY(id)
);

CREATE TABLE topic_categories (
	topic_id INT(11) UNSIGNED DEFAULT NULL,
	category_id INT(11) UNSIGNED DEFAULT NULL,
	
	FOREIGN KEY (topic_id) REFERENCES topics(id),
	FOREIGN KEY (category_id) REFERENCES categories(id),
	
	PRIMARY KEY(topic_id, category_id)
);

CREATE TABLE answers (
	id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	content BLOB NOT NULL,
	
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	
	topic_id INT(11) UNSIGNED NOT NULL,
	creator_id INT(11) UNSIGNED NOT NULL,
	
	anonymous BOOLEAN DEFAULT FALSE,
	
	FOREIGN KEY (creator_id) REFERENCES users(id),
	FOREIGN KEY (topic_id) REFERENCES topics(id),
	PRIMARY KEY(id)
);

/**

Subsciptions System

****/
CREATE TABLE topic_subscriptions (
	topic_id INT(11) UNSIGNED NOT NULL,
	user_id INT(11) UNSIGNED NOT NULL,
	
	FOREIGN KEY (topic_id) REFERENCES topics(id),
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY(topic_id, user_id)
);

CREATE TABLE category_subscriptions (
	category_id INT(11) UNSIGNED NOT NULL,
	user_id INT(11) UNSIGNED NOT NULL,
	
	FOREIGN KEY (category_id) REFERENCES categories(id),
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY(category_id, user_id)
);

/**

Voting System

****/
CREATE TABLE topic_votes (
	topic_id INT(11) UNSIGNED NOT NULL,
	user_id INT(11) UNSIGNED NOT NULL,
	positive BOOLEAN NOT NULL,
	
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (topic_id) REFERENCES topics(id),
	PRIMARY KEY(topic_id, user_id)
);

CREATE TABLE answer_votes (
	answer_id INT(11) UNSIGNED NOT NULL,
	user_id INT(11) UNSIGNED NOT NULL,
	positive BOOLEAN NOT NULL,
	
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (answer_id) REFERENCES answers(id),
	PRIMARY KEY(answer_id, user_id)
);

/**

Friends/Blocks/Chat System

****/

CREATE TABLE chat_presence (
	user_id INT(11) UNSIGNED NOT NULL,
	active_login_code VARCHAR(255) NOT NULL,
	
	chat_ip VARCHAR(15) NOT NULL,
	chat_port INT(11) NOT NULL,
	
	last_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY(user_id, active_login_code)
);

CREATE INDEX chat_presence_update_date
	ON chat_presence (last_updated DESC);

CREATE INDEX chat_presence_ip_port
	ON chat_presence (chat_ip, chat_port);

CREATE TABLE user_blocks (
	blocking_user_id INT(11) UNSIGNED NOT NULL,
	blocked_user_id INT(11) UNSIGNED NOT NULL,
	
	FOREIGN KEY (blocking_user_id) REFERENCES users(id),
	FOREIGN KEY (blocked_user_id) REFERENCES users(id),
	PRIMARY KEY(blocked_user_id, blocking_user_id)
);


------------------------
------------------------
-- Insert some data   --
------------------------
------------------------


INSERT INTO categories (name) VALUES
	('Physical Layer'),
	('Data Link Layer'),
	('Network Layer'),
	('Transport Layer'),
	('Application Layer'),
	
	('HTTP'),
	('HTTPS'),
	('FTP'),
	('SFTP'),
	('DNS'),
	('SSH'),
	('TELNET'),
	('SMTP'),
	('POP3'),
	('IMAP'),
	('SSL'),
	('IP'),
	('ICMP'),
	('ATM'),
	('TCP'),
	('UDP'),
	
	('traceroute'),
	('ipconfig'),
	('whois'),
	('ping'),
	('dig'),
	
	('Wireshark'),
	('Nmap'),
	
	('IP Address'),
	('Subnet'),
	('Subnet Mask'),
	('Fragmentation'),
	('Pipelining'),
	('Checksum'),
	
	('Socket'),
	('Network Programming'),
	('Java'),
	('Python'),
	('C/C++'),
	
	('RTT'),
	('Propagation Delay'),
	('Queueing Delay'),
	('Processing Delay'),
	('Transmission Delay'),
	('End to End Delay'),
	('Timing Diagram'),
	
	('Headers'),
	('Cookies'),
	('User Agent'),
	('Web Caching'),
	('GET'),
	('POST'),
	('PUT'),
	('DELETE'),
	('OPTIONS'),
	('HEAD'),
	
	('Discussion'),
	('Assignment'),
	('Announcement'),
	('Miscellaneous')
;
